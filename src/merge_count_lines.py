#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 10:16:33 2018

@author: luiz fernando giolo alves
"""

import pandas as pd
import sys
import time
import os


#Pegando Tolerancia de MZ do segundo argumento, se nada for colocado, usar 0.02
try:
    massTol = float(sys.argv[2])
except:
    massTol = 0.02

#Pegando Tolerancia de RT do terceiro argumento, se nada for colocado, usar 5
try:
    rtTol = float(sys.argv[3])
except:
    rtTol = 5

#Pegando caminho de saida no quarto argumento
try:
    outPath = str(sys.argv[4])
except:
    outPath = ""

#Importando csv do mscluster
#Como a primeira linha tem a bioativadade, pular a primeira linha
#<<<<<<< HEAD
#=======
#csvFile = ("/home/luizgiolo/Documentos/NP3/Edge Table/esseMenor3.csv")
#>>>>>>> master
#csvFile = ("/home/luizgiolo/Documentos/NP3/SandBox/Merge/181.csv")
csvFile = str(sys.argv[1])

dfMSCluster = pd.read_csv(csvFile)

print("\nTrabalhando com o arquivo \""+str(csvFile)+"\"")
print("Vamos usar uma tolerância para mz de "+str(massTol))
print("e "+str(rtTol)+" segundos para tempo de retenção")



def rtstrClean(string):
    try:
        inicio = string.find(":")
    except:
        #print("Nao foi possivel encontrar o \":\"")
        return string
    inicio += 1
    string = string[inicio:]
    string.strip()
    return float(string)


"""
Busca no Data Frame as massas de m/z similar dentro do intervalo de RT.
Retorna uma lista com vários data frames com massas similares para serem somados
"""    
def buscaAnalogo(dataFrame):
    #Listas auxiliares, a temp é para o loop, a toMerge guarda as linhas que deverão ser juntadas
    #usados guarda o index das linhas já guardadas
    tempList = []
    toMerge = []
    usados = []
    #Guardando o numero de linhas da tabela

    for i in dataFrame.index:
        #Verfica se o numero de index ja foi usado
        if not (i in usados):
            #Se não foi então ele sera usado então indexa na lista de já usados
            usados.append(i)
            
            #Indexa a linha do dataFrame na lsta temporaria
            tempList.append(dataFrame[dataFrame.index == i])
            
            #Guarda o mz e o rt da entidade desta linha
            mz = dataFrame["mzConsensus"][i]
            rt = dataFrame["rtMean"][i]
            rtmn = dataFrame["rtMin"][i]
            rtmx = dataFrame["rtMax"][i]

            
            #percorre o resto do dataFrame buscando massas similares em mz e rt
            tempDataFrame = dataFrame[(dataFrame["mzConsensus"]<= mz + massTol) & (dataFrame["mzConsensus"]>= mz - massTol)]
            for j in tempDataFrame["msclusterID"]:
                rtj = tempDataFrame["rtMean"][j]
                rtjmn = tempDataFrame["rtMin"][i]
                rtjmx = tempDataFrame["rtMin"][i]
                #if (abs(rt - rtj) <= rtTol) and (not(j in usados)):
                #TODO verificar 
                if ((rtj <= rtmx) and (rtj >= rtmn) and (not(j in usados))):
                    if ((rtjmn >= rtmn-rtTol) and (rtjmx <= rtmx+rtTol)):
                        tempList.append(tempDataFrame[tempDataFrame.index == j])
                        usados.append(j)
        # Se a lista temporaria tiver mais que uma entidade, então tera que ser mesclada no futuro
        if len(tempList) > 1:
            toMerge.append(tempList)
        tempList = []
    toMergeDF = []

    #Anexa as linhas que foram mescladas
    for conjunto in toMerge:
        dfAux = conjunto[0]
        for i in range(1,len(conjunto)):
            dfAux = dfAux.append(conjunto[i])
        toMergeDF.append(dfAux)
    
    return toMergeDF


"""
Soma as linhas de um DataFrame
"""    
def mergeFrames(dataFrame):
    #As colunas marcadas são de parâmetros que não serão somados
    naoSoma = ["msclusterID", "mzConsensus", "rtMean", "rtMin", "rtMax"]
    #Dicionário que irá criar o dataFrame com as linhas somadas ou com a média
    dictFrame = {}
    #Passando por todas as colunas do dataFrame
    for coluna in dataFrame.columns:
        if coluna in naoSoma:
            #Na coluna do ID, pega-se o menor valor
            if coluna == "msclusterID":
                dictFrame[coluna] = dataFrame[coluna].min()
            #Na coluna do mz, pega-se a média ponderada pelo numero de espectros
            if coluna == "mzConsensus":
                sumMass = 0
                sumMassPond = 0
                sumSpect = 0
                i=0
                for ind in dataFrame.index:
                    mz = dataFrame["mzConsensus"][ind]
                    nspet = dataFrame["numSpectra"][ind]
                    sumMassPond += mz*nspet
                    sumMass += mz
                    sumSpect += nspet
                    i += 1
                try:
                    quocient = sumMassPond/sumSpect
                except:
                    quocient = sumMass/i
                dictFrame[coluna] = round(quocient,3)
            #Na coluna do rt, pega-se a média ponderada pelo numero de espectros
            if coluna == "rtMean":
                sumRt = 0
                sumRtPond = 0
                sumSpect = 0
                i = 0
                for ind in dataFrame.index:
                    rt = dataFrame["rtMean"][ind]
                    nspet = dataFrame["numSpectra"][ind]
                    sumRt += rt
                    sumRtPond += rt*nspet
                    sumSpect += nspet
                    i += 1
                try:
                    quocient = sumRtPond/sumSpect
                except:
                    quocient = sumRt/i

                rtResult = round(quocient,3)
                dictFrame[coluna] = rtResult
            #Coluna rt min (por enquanto) pegar o menor valor
            if coluna == "rtMin":
                dictFrame[coluna] = dataFrame[coluna].min()
            #Coluna rt max (por enquanto) pegar o maior valor
            if coluna == "rtMax":
                dictFrame[coluna] = dataFrame[coluna].max()
        #As demais colunas
        else:
            #se tiver COR_ no título, pega-se o maior valor
            if "COR_" in coluna:
                dictFrame[coluna] = dataFrame[coluna].max()
            #Se não, simplesmente soma-se as linhas
            else:
                dictFrame[coluna] = dataFrame[coluna].sum()
    
    #Gera o Data Frame com as linhas somadas
    return pd.DataFrame(dictFrame, index=[dictFrame["msclusterID"]])

"""
A função abaixo chama as outras funções na ordem necessária para fazer toda
a busca de linhas similares, soma e mescla

1) Busca os analogos e retorna a lista de dataframes

2) Soma as linhas dos dataframes da lista do passo 1
    
3) Busca na lista de DataFrames quais são as linhas devem ser deletadas e as deleta
    
4) Incorpora as linhas foram mescladas no DataFrame e ordena pelo msclusdterID novamente
    

"""

def completeRoutine(dataFrame):
    #1
    #TODO tirar tempo depois
    t_p1_i = time.time()
    
    toJoin = buscaAnalogo(dataFrame)
    dfList = []
    
    #2
    for frame in toJoin:
        dfList.append(mergeFrames(frame))
    #3    
    toDrop = []
    for frame in toJoin:
        for indice in frame.index:
            toDrop.append(indice)
    
    newMSCluster = dataFrame.drop(toDrop)

    #TODO tirar tempo depois
    t_p1_f = time.time()
    minute = int((t_p1_f-t_p1_i)//60)
    seg = int((((t_p1_f-t_p1_i)/60)-minute)*60)
    print(".\n..\n...\n....")
    print("tempo leitura e soma de linhas "+str(minute)+" minutos "+str(seg)+"segundos")

    
    #4
    #TODO tirar tempo depois
    t_p2_i = time.time()
    for row in dfList:
        newMSCluster = newMSCluster.append(row)
        
    newMSCluster = newMSCluster.sort_values(by=["msclusterID"])
    
    #TODO tirar tempo depois
    t_p2_f = time.time()
    minute = int((t_p2_f-t_p2_i)//60)
    seg = int((((t_p2_f-t_p2_i)/60)-minute)*60)
    print(".\n..\n...\n....")
    print("tempo leitura e soma de linhas "+str(minute)+" minutos "+str(seg)+"segundos")
    return newMSCluster
        
    
tempoInicio = time.time()

#Seta clusterSize para 1

dfMSCluster["clusterSize"]=1


#Primeira Busca
print("....\n...\n..\n.")
print("Realizando primeira busca")
newCluster = completeRoutine(dfMSCluster)

verificador = True
lines = dfMSCluster.shape[0]
lines_post = newCluster.shape[0]

if lines == lines_post:
    verificador = False

dfFinal = newCluster
contador = 1

#Repete o algoritmo enquanto tiver mzs similares
#TODO encontrar um jeito de não fazer a ultima busca
while verificador:
    lines = dfFinal.shape[0]
    print("....\n...\n..\n.")
    print("Realizando "+str(contador+1)+"a busca")
    dfFinal = completeRoutine(dfFinal)
    lines_post = dfFinal.shape[0]
    contador += 1
    if lines == lines_post:
        verificador = False

print("A " +str(contador)+"a foi a ultima busca")



#TODO Criar uma função para FLAGS


#Adcionar Blank Flag - BFLAG
dictBflag = {}
listaID = []
listaPM = []

for ind in dfMSCluster[dfMSCluster.BLANKS_TOTAL>0].index:
    listaID.append(dfMSCluster["msclusterID"][ind])
    listaPM.append(dfMSCluster["mzConsensus"][ind])

for i in range(len(listaID)):
    dictBflag[listaID[i]]=listaPM[i]

dfBFlag = pd.DataFrame({"msclusterID":listaID, "mzConsensus":listaPM })
listaBFlagID = []
listaBFlagBool = []
for ind in dfFinal.index:
    sup = dfFinal["mzConsensus"][ind]+massTol
    inf = dfFinal["mzConsensus"][ind]-massTol
    if len(dfBFlag[(dfBFlag.mzConsensus < sup) & (dfBFlag.mzConsensus > inf)])>0:
        listaBFlagID.append(dfFinal["msclusterID"][ind])
        listaBFlagBool.append(True)
    else:
        listaBFlagID.append(dfFinal["msclusterID"][ind])
        listaBFlagBool.append(False)


dfNewCol = pd.DataFrame({"msclusterID":listaBFlagID, "BFLAG":listaBFlagBool})
dfNewCol = dfNewCol.set_index("msclusterID")
frames = [dfFinal, dfNewCol]

dfFinal = dfFinal.assign(BFLAG=dfNewCol["BFLAG"])


#Adicionar Control Flag - CFLAG


dictCflag = {}
listaIDC = []
listaPMC = []

for ind in dfMSCluster[dfMSCluster.CONTROLS_TOTAL>0].index:
    listaIDC.append(dfMSCluster["msclusterID"][ind])
    listaPMC.append(dfMSCluster["mzConsensus"][ind])
            
for i in range(len(listaIDC)):
    dictCflag[listaIDC[i]]=listaPMC[i]
    

dfCFlag = pd.DataFrame({"msclusterID":listaIDC, "mzConsensus":listaPMC })
listaCFlagID = []
listaCFlagBool = []
for ind in dfFinal.index:
    sup = dfFinal["mzConsensus"][ind]+massTol
    inf = dfFinal["mzConsensus"][ind]-massTol
    if len(dfCFlag[(dfCFlag.mzConsensus < sup) & (dfCFlag.mzConsensus > inf)])>0:
        listaCFlagID.append(dfFinal["msclusterID"][ind])
        listaCFlagBool.append(True)
    else:
        listaCFlagID.append(dfFinal["msclusterID"][ind])
        listaCFlagBool.append(False)

dfNewCol = pd.DataFrame({"msclusterID":listaCFlagID, "CFLAG":listaCFlagBool})
dfNewCol = dfNewCol.set_index("msclusterID")
frames = [dfFinal, dfNewCol]

dfFinal = dfFinal.assign(CFLAG=dfNewCol["CFLAG"])



#TODO tirar esse print ao terminar o script
tempoFimTotal = time.time()
deltaT2 = (tempoFimTotal - tempoInicio)
minute = int(deltaT2 // 60)
seg = int(((deltaT2/60)-minute)*60)
print("\n.....")
print("Demoramos "+str(minute)+" minutos e "+str(seg)+" segundos para execução Total")
#print("Precisamos juntar "+str(contador)+" vezes as linhas")

print("\n.....")
print("Juntamos um total de "+str(abs(dfMSCluster.shape[0]-dfFinal.shape[0]))+" linhas.")
print("\n.....")
print("Exportando arquivo csv")


listadasColunas = []
for name in dfMSCluster.columns:
    if name == "numSpectra":
        listadasColunas.append(name)
        listadasColunas.append("BFLAG")
        listadasColunas.append("CFLAG")
    else:
        listadasColunas.append(name)
                

fileName = csvFile[:(len(csvFile)-4)]

if outPath == "":
    fileName = outPath+fileName + "_MergedLines_MZtol_"+str(massTol)+"_RTtol_"+str(rtTol)+".csv"
    dfFinal.to_csv(fileName, index = False, columns = listadasColunas)
else:
    #barRef = 0
    #for i in range(len(fileName)):
    #    if (fileName[i]=="/" | fileName[i]=="\\"):
    #        barRef = i
    fileName = os.path.basename(fileName)
    fileName = outPath+fileName + "_MergedLines_MZtol_"+str(massTol)+"_RTtol_"+str(rtTol)+".csv"
    dfFinal.to_csv(fileName, index = False, columns = listadasColunas)
        
print("....\n...\n..\n.")
print("PRONTO!!!!")
print("Procure pelo arquivo \""+fileName+"\"")





