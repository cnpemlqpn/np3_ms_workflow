#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// [[Rcpp::export]]
double normDotProduct(std::vector<double> peaks_A, std::vector<double> ints_A, 
                      std::vector<double> peaks_B, std::vector<double> ints_B, double bin_size) {
  int n_A = peaks_A.size(), n_B = peaks_B.size();
  
  //Rcout << n_A;
  
  double sum_ints_A = 0.0, sum_ints_B = 0.0, top_sum = 0.0; // sum(sa^2); sum(sb^2); sum(sai*sbi);
  int idxA = 0, idxB = 0;
  
  while (idxA < n_A && idxB < n_B)
  {
    const double diff = peaks_A[idxA] - peaks_B[idxB];
    //Rcout << diff;
    
    if (diff <= 0) // pB > pA; increment pA
    {
      if (diff + bin_size >= 0)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        idxA++;
      }
    } else {
      if (diff <= bin_size)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        idxB++;
      }
    }
  }
  
  for (int i = idxA; i < n_A; i++) {
    sum_ints_A += ints_A[i] * ints_A[i];
  }
  for (int i = idxB; i < n_B; i++) {
    sum_ints_B += ints_B[i] * ints_B[i];
  }
  
  return (top_sum / sqrt(sum_ints_A * sum_ints_B));
}

// [[Rcpp::export]]
double normDotProductTrim(std::vector<double> peaks_A, std::vector<double> ints_A, 
                      std::vector<double> peaks_B, std::vector<double> ints_B, double bin_size, double trim_mz) {
  int n_A = peaks_A.size(), n_B = peaks_B.size();
  
  //Rcout << n_A;
  
  double sum_ints_A = 0.0, sum_ints_B = 0.0, top_sum = 0.0; // sum(sa^2); sum(sb^2); sum(sai*sbi);
  int idxA = 0, idxB = 0;
  
  while (idxA < n_A && idxB < n_B && peaks_A[idxA] < trim_mz && peaks_B[idxB] < trim_mz)
  {
    const double diff = peaks_A[idxA] - peaks_B[idxB];
    //Rcout << diff;
    
    if (diff <= 0) // pB > pA; increment pA
    {
      if (diff + bin_size >= 0)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        idxA++;
      }
    } else {
      if (diff <= bin_size)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        idxB++;
      }
    }
  }
  
  for (int i = idxA; i < n_A; i++) {
    if (peaks_A[i] > trim_mz)
      break;
    sum_ints_A += ints_A[i] * ints_A[i];
  }
  for (int i = idxB; i < n_B; i++) {
    if (peaks_B[i] > trim_mz)
      break;
    sum_ints_B += ints_B[i] * ints_B[i];
  }
  
  return (top_sum / sqrt(sum_ints_A * sum_ints_B));
}


/***
 #R
# Rcpp::sourceCpp('src/read_mgf_peak_list_R.cpp')

test_compare_sp <- function(mz1, int1, mz2, int2, binSize, trim = -1, scale = 1) {
  sp1 <- joinAdjPeaksScalee(mz1, int1, binSize, trim, scale)
  sp2 <- joinAdjPeaksScalee(mz2, int2, binSize, trim, scale)
  
  
  
  MSnbase::compareSpectra(new("Spectrum2", mz = mz1, intensity = int1),
                 new("Spectrum2", mz = mz2, intensity = int2),
                 fun = "dotproduct", binSize = binSize)
  
  simnp3 <- round(normDotProduct(sp1[[1]], sp1[[2]], sp2[[1]], sp2[[2]], binSize), 3)
  simmsnbase <-  round(MSnbase::compareSpectra(new("Spectrum2", mz = mz1, intensity = int1),
                                        new("Spectrum2", mz = mz2, intensity = int2),
                                        fun = "dotproduct", binSize = binSize), 3)
  message("norm dot = ", simnp3,
          " compare spectra = ", simmsnbase)
  if (simnp3 == simmsnbase)
    return(TRUE)
  else 
    return(FALSE)
}
# equal
test_compare_sp(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,5), 0.025)
# one intersection equal int
test_compare_sp(c(1,2,3), c(1,2,3), c(3,4,5), c(3,4,5), 0.025)
# one intersection equal int
test_compare_sp(c(1,2,3,5), c(1,2,3,5), c(4,5), c(4,5), 0.025)
# one intersection not equal int
test_compare_sp(c(1,2,3), c(1,2,3), c(3,4,5), c(5,4,3), 0.025)
# middle intersection equal int
test_compare_sp(c(1,2,3,4), c(1,2,3,4), c(2,3), c(2,3), 0.025)
# middle intersection not equal int
test_compare_sp(c(1,2,3,4), c(1,2,3,4), c(2,3), c(4,2), 0.025)
# one intersection not equal int
test_compare_sp( c(3,4,5), c(5,4,3),c(1,2,3), c(1,2,3), 0.025)
# no intersection
test_compare_sp(c(1,2), c(1,2), c(3,4,5), c(3,4,5), 0.025)
# no intersection
test_compare_sp(c(3,4,5), c(3,4,5),c(1,2), c(1,2),  0.025)
# all equal diff ints
test_compare_sp(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,5), c(5,4,3,2,1), 0.025)
# one all equal, diff ints
test_compare_sp(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3), c(5,4,3), 0.025)
# one all equal, equal ints
test_compare_sp(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3), c(1,2,3), 0.025)
 
# test equal no shift
normDotProductTrimShift(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,5), 0.025, 1.0) == 1
# test equal with shift in the end
normDotProductTrimShift(c(1,2,3,4,5), c(1,2,3,4,5), c(1,2,3,4,6), c(1,2,3,4,5), 0.025, 1.0) == 1
# test equal with shift in the end and in the beginning
normDotProductTrimShift(c(1,2,3,4,7), c(1,2,3,4,5), c(0.024,2,3,5,8), c(1,2,3,4,5), 0.025, 1.0) == 1
# test no intersection
 normDotProductTrimShift(c(3), c(5),c(1,2), c(1,2), 0.025, 0) == normDotProductTrimShift(c(3,4,5), c(3,4,5),c(1,2), c(1,2), 0.025, 0)
# test one intersection 
 normDotProductTrimShift(c(1,2,3), c(1,2,3), c(3), c(3),0.025, 0) == normDotProductTrimShift(c(3), c(3),c(1,2,3), c(1,2,3), 0.025, 0)
# test all equal with trim
 normDotProductTrimShift(c(3), c(3), c(3,4,5), c(3,4,5), 0.025, 0) == normDotProductTrimShift(c(3), c(3), c(3), c(3), 0.025, 0)
 ***/ 


// TODO # commom peaks gives a bonus to sim if greater than 4 and a percentage of total sum?

// Trim by the minimum maximum peak and by the maximum minimum peak of both specs, 
// maintaining the total norm with no need to computation (the final equation would simplify them)
// Match exactly peak masses and for the remaining try to match with shifted peak masses
// [[Rcpp::export]]
double normDotProductTrimShift(std::vector<double> peaks_A, std::vector<double> ints_A,
                      std::vector<double> peaks_B, std::vector<double> ints_B,
                      double bin_size, double mzShift) {
  int n_A = peaks_A.size(), n_B = peaks_B.size(), i;
  
  //Rcout << n_A;
  std::vector<int> idxA_nomatch, idxB_nomatch;
  
  // trim both peaks by the smallest one compared to the right side and then
  // use this one as mask range to the other
  // no need to fix the total sum to keep the norm
  // trim upper masses
  if (peaks_A[n_A-1] > peaks_B[n_B-1] + bin_size + mzShift) {
    // trim A upper
    for (i = n_A-2; i >= 0; i--) {
      if (peaks_A[i] < peaks_B[n_B-1] + bin_size + mzShift) {
        break;
      } 
    }
    peaks_A.erase(peaks_A.begin()+i+1, peaks_A.begin()+n_A);
    ints_A.erase(ints_A.begin()+i+1, ints_A.begin()+n_A);
    n_A = peaks_A.size();
    
    // trim A lower
    if (n_A > 0 && peaks_A[0] < peaks_B[0] - bin_size - mzShift) {
      for (i = 1; i < n_A; i++) {
        if (peaks_A[i] > peaks_B[0]  - bin_size - mzShift) {
          break;
        }
      }
      peaks_A.erase(peaks_A.begin(), peaks_A.begin()+i);
      ints_A.erase(ints_A.begin(), ints_A.begin()+i);
      n_A = peaks_A.size();
    }
  } else if (peaks_B[n_B-1] > peaks_A[n_A-1] + bin_size + mzShift){
    // trim B upper
    for (i = n_B-2; i >= 0; i--) {
      if (peaks_B[i] < peaks_A[n_A-1] + bin_size + mzShift) {
        break;
      } 
    }
    peaks_B.erase(peaks_B.begin()+i+1, peaks_B.begin()+n_B);
    ints_B.erase(ints_B.begin()+i+1, ints_B.begin()+n_B);
    n_B = peaks_B.size();
    
    // trim B lower
    if (n_B > 0 && peaks_B[0] < peaks_A[0] - bin_size - mzShift) {
      for (i = 1; i < n_B; i++) {
        if (peaks_B[i] > peaks_A[0]  - bin_size - mzShift) {
          break;
        }
      }
      peaks_B.erase(peaks_B.begin(), peaks_B.begin()+i);
      ints_B.erase(ints_B.begin(), ints_B.begin()+i);
      n_B = peaks_B.size();
    }
  }
  
  double sum_ints_A = 0.0, sum_ints_B = 0.0, top_sum = 0.0; // sum(sa^2); sum(sb^2); sum(sai*sbi);
  int idxA = 0, idxB = 0;
  
  // match equal peak masses
  while (idxA < n_A && idxB < n_B)
  {
    const double diff = peaks_A[idxA] - peaks_B[idxB];
    //Rcout << diff;
    
    if (diff <= 0) // pB > pA; increment pA
    {
      if (diff + bin_size >= 0)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        idxA_nomatch.push_back(idxA);
        //sum_ints_A += ints_A[idxA] * ints_A[idxA];
        idxA++;
      }
    } else {
      if (diff <= bin_size)
      {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
        sum_ints_B += ints_B[idxB] * ints_B[idxB];
        top_sum += ints_A[idxA] * ints_B[idxB];
        idxA++;
        idxB++;
      } else {
        //sum_ints_B += ints_B[idxB] * ints_B[idxB];
        idxB_nomatch.push_back(idxB);
        idxB++;
      }
    }
  }
  
  // add no match indexes to the list
  for (i = idxA; i < n_A; i++) {
    idxA_nomatch.push_back(i);
  }
  n_A = idxA_nomatch.size();
  for (i = idxB; i < n_B; i++) {
    idxB_nomatch.push_back(i);
  }
  n_B = idxB_nomatch.size();
  
  // Rcout << "nA " << n_A << "\n";
  // Rcout << "nB " << n_B << "\n";
  
  // try to match the not matched masses with a mzShift, if it is greater than the bin size
  if (mzShift > bin_size) {
    for (i = 0; i < n_A && n_B > 0; i++) {
      idxA = idxA_nomatch[i];
      int j = 0, matched = 0;
      
      // find a match on peaks_B for idxA
      while (j < n_B && mzShift > 0.0) {
        idxB = idxB_nomatch[j];
        
        double diff = peaks_A[idxA]-peaks_B[idxB];
        if (diff <= 0) // pB > pA; increment pA
        {
          diff = abs(diff + mzShift);
          if (diff <= bin_size) // match with mzShift
          {
            sum_ints_A += ints_A[idxA] * ints_A[idxA];
            sum_ints_B += ints_B[idxB] * ints_B[idxB];
            top_sum += ints_A[idxA] * ints_B[idxB];
            idxB_nomatch.erase(idxB_nomatch.begin() + j);
            n_B--;
            matched = 1;
            break;
          } else if (diff > mzShift) {
            break; // no match for idxA
          } else {
            j++;
          }
        } else { // pA > pB; increment pB
          diff = abs(diff - mzShift);
          if (diff <= bin_size) // match with mzShift
          {
            sum_ints_A += ints_A[idxA] * ints_A[idxA];
            sum_ints_B += ints_B[idxB] * ints_B[idxB];
            top_sum += ints_A[idxA] * ints_B[idxB];
            idxB_nomatch.erase(idxB_nomatch.begin() + j);
            n_B--;
            matched = 1;
            break;
          } else {
            j++;
          }
        }
      }
      if (matched == 0) {
        sum_ints_A += ints_A[idxA] * ints_A[idxA];
      }
    }
  } else {
    i = 0;
  }
  
  // TODO add && < n_B, usar o minimo numero de massas
  // heuristica para ordenar e uar as menos intensas se n_B for menor que N_A, se nao nao precisa pq vai usar tudo de todo jeito
  // adiciona os picos sem match
  for (;i < n_A; i++) {
    sum_ints_A += ints_A[idxA_nomatch[i]] * ints_A[idxA_nomatch[i]];
  }
  
  for (i = 0; i < n_B; i++) {
    sum_ints_B += ints_B[idxB_nomatch[i]] * ints_B[idxB_nomatch[i]];
  }
  
  // The multipliers to maintain the norm would cancel each other here eliminating the need to compute it
  if (top_sum > 0.0) {
    return (top_sum / sqrt(sum_ints_A * sum_ints_B));
  } else {
    return top_sum;
  }
}

// mgf <- readMgfPeaksList("/home/crisfbazz/Documents/CNPEM/ms_entity_annotation/test/test_analogs.mgf", 0.05, -1, 0.5)
// plotSpectrum <- function(mgf, i, j, tol) {
//   MSnbase::plot(new("Spectrum2", mz = mgf$MZS[[i]], intensity = mgf$INTS[[i]], centroided = T),
//                 new("Spectrum2", mz = mgf$MZS[[j]], intensity = mgf$INTS[[j]], centroided = T),
//                 tolerance = tol)
// }
// 
// normSpecs <- function(mgf, i, j, tol, shift) {
//   normDotProductTrimShift(mgf$MZS[[i]], mgf$INTS[[i]],
//                           mgf$MZS[[j]], mgf$INTS[[j]], tol, shift)
// }
// normSpecs(mgf, 1, 2, 0.05, abs(426.23395- 442.23026))
// normSpecs(mgf, 3, 4, 0.05, abs(350.21741-522.29138))



