#include <Rcpp.h>
#include <math.h>
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//
//ISO_MASS = 1.0033;

// TODO add full sanity check after norm
// [[Rcpp::export]]
std::vector<std::vector<double> > joinAdjPeaksScalee(std::vector<double> peaks, std::vector<double> ints, 
                                      double bin_size, double trim_mz, double scale_method) {
  int n = peaks.size();
  std::vector<double> ints_joined;
  
  int prev = 0;
  double totalIntensity = ints[prev];
  
  for(int i = prev+1; i < n; ++i) {
    // dont use mz peaks above the trim value
    if (trim_mz != -1 && peaks[i] > trim_mz) {
      break;
    }
    totalIntensity += ints[i];
    if (peaks[i] - peaks[prev] <= bin_size) {
      // join peaks proportional to their intensity
      const double intensitySum = ints[prev] + ints[i];
      const double ratio = ints[prev]/intensitySum;
      const double newMz = peaks[prev]*ratio + peaks[i]*(1-ratio);
      
      peaks[prev] = newMz;
      ints[prev] = intensitySum;
    } else {
      ints_joined.push_back(ints[prev]);
      prev++;
      peaks[prev] = peaks[i];
      ints[prev] = ints[i];
    }
  }
  
  ints_joined.push_back(ints[prev++]);
  
  // obtain min intensity to filter very low intensity peaks (without a window)
  sort(ints_joined.begin(), ints_joined.end());
  double min_intensity;
  if (ints_joined.size() > 10) {
    min_intensity = ints_joined[2*prev/3]*0.1;
  } else {
    min_intensity = ints_joined[2*prev/3]*0.01; // do not filter too much spectra with few peaks
  }
  
  // rmv intensities bellow the min
  for (int i = 0; i < prev; i++)
  {
    if (ints_joined[i] <= min_intensity)
    {
      totalIntensity = totalIntensity - ints_joined[i];
    } else {
      break;
    }
  }
  
  // store the joined peaks concatenated with the intensities
  std::vector<double> out_p, out_i;
  
  double multVal = 1000.0 / totalIntensity;
  for (int i = 0; i < prev; i++) {
    if (ints[i] > min_intensity)
    {
      out_p.push_back(peaks[i]);
      
      // compute intensity
      if (scale_method == 0.0)
      {
        out_i.push_back(1.0 + log(1.0 + multVal * ints[i])); // ln scale
      } else {
        out_i.push_back(pow(multVal * ints[i], scale_method)); // pow  scale
      }
    }
  }
  
  std::vector<std::vector<double> > out;
  out.push_back(out_p);
  out.push_back(out_i);
  
  return out;
}

// if trim_mz == -1 do not trim spectra; if trim_mz != -1 trim spectra by precursor mz + 5*iso mass + bin_size
// scale_factor: 0 - ln; (0,] expoent of pow; and if -1 no join nor scale nor trim nor norm
// [[Rcpp::export]]
List readMgfPeaksList(std::string filePath, double bin_size, double trim_mz, double scale_factor) {
  FILE*  mgfStream = fopen(filePath.c_str(),"r");
  
  double ISO_MASS = 1.0033;
  std::vector<double> mz, inty, mz_prec;
  std::vector<int> scans;
  std::vector< std::vector<double> > mzs, ints, spec_scale;
  
  int scanNumber = -1;
  double mOverZ = -1.0;
  
  char buffer[256];
  //assert(fileType_ == IFT_MGF);
  
  while (fgets(buffer, 256, mgfStream))
  {
    if (strncmp(buffer,"BEGIN IONS",10))
      continue;
    break;
  }
  
  long positionInFile = ftell(mgfStream);
  //float precursorIntensity_ = 0.0;
  //float firstPeakIntensity=0.0;
  
  if (positionInFile < 0) {
    Rcpp::stop("Bad skip position in mgf file! This can often be corrected by running unix2dos (or vice versa if appropriate)\n");
  }
  
  // read header info and first peak
  while (true)
  {
    if (! strncmp(buffer,"END IONS",7))
    {
      if (trim_mz != -1)
        trim_mz = mOverZ + 5*ISO_MASS + bin_size;
      
      //Rcpp::Rcout << mOverZ << " \n";
      
      if (scale_factor != -1)
      {
        spec_scale = joinAdjPeaksScalee(mz, inty, bin_size, trim_mz, scale_factor);
        
        // append current peak list to the total list
        mzs.push_back(spec_scale[0]);
        ints.push_back(spec_scale[1]);
      } else {
        // no join nor scale nor norm
        mzs.push_back(mz);
        ints.push_back(inty);
      }
      scans.push_back(scanNumber);
      mz.clear();
      inty.clear();
      
      // got to next ion
      while (fgets(buffer, 256, mgfStream))
      {
        if (strncmp(buffer,"BEGIN IONS",10))
          continue;
        break;
      }
    }
    // eof
    if( ! fgets(buffer, 256, mgfStream))
      break;
    
    //Rcpp::Rcout << buffer << " \n";
    
    if (!strncmp(buffer,"BEGIN IONS",10))
      continue;
    
    if (! strncmp(buffer,"TITLE=",6) )
    {
      continue;
      // int len = strlen(buffer)-1;
      // buffer[len]='\0';
      // if (buffer[len-1]=='\r' || buffer[len-1]=='\n' )
      //   buffer[len-1]='\0';
      // 
      // string titleStr = buffer + 6;
      // setTitle(titleStr);
      // 
      // // see if title includes scan number information.
      // // this works only if the title ends with: .xxxx.yyyy.d.dta
      // // e.g., MyMSMSData.2000.2000.2.dta
      // // if the title has this format then scanNumber is set to xxxx
      // if (scanNumber_<0)
      // {
      //   len = title_.length();
      //   if (len>7 &&
      //       title_[len-1] == 'a' && title_[len-2] == 't' && title_[len-3]=='d' && 
      //       title_[len-6] == '.' && title_[len-4]== '.')
      //   {
      //     int pos = len-7;
      //     int numDots = 0;
      //     
      //     while (pos>0 && numDots<2)
      //     {
      //       --pos;
      //       if (title_[pos] == '.')
      //         ++numDots;
      //     }
      //     
      //     if (numDots == 2)
      //     {
      //       string scanString = title_.substr(pos+1,len-7-pos);
      //       int i;
      //       for (i=0; i<scanString.length(); i++)
      //         if (scanString[i] == '.')
      //         {
      //           scanString[i]=' ';
      //           break;
      //         }
      //         
      //         istringstream iss(scanString);
      //         int scan1=-1, scan2=-1;
      //         iss >> scan1 >> scan2;
      //         if (scan1 <= scan2 && scan1>0)
      //           scanNumber_ = scan1;
      //     }
      //   }
      //   continue;
      // }
    }
    else if (! strncmp(buffer,"SEQ=",4) || ! strncmp(buffer,"PEPSEQ=",7)
               || ! strncmp(buffer,"NUM_PEAKS=",10) || ! strncmp(buffer,"SQS=",4))
    {
      continue;		
    }
    else if (! strncmp(buffer,"SCAN=",5) )
    {
      //Rcpp::Rcout << "scan";
      if (sscanf(buffer+5,"%d",&scanNumber) != 1)
      {
        Rcpp::stop("Error: couldn't read scan number from mgf file!\n");
      }
      continue;
    }
    else if (! strncmp(buffer,"SCANS=",6) ) // this is the offical MGF field, only the first number is kept
    {
      //Rcpp::Rcout << "scans";
      if (sscanf(buffer+6,"%d",&scanNumber) != 1)
      {
        Rcpp::stop("Error: couldn't read scan number from mgf file!\n");
      }
      continue;
    }
    else if (! strncmp(buffer,"RT=",3) )
    {
      // if (sscanf(buffer+3,"%f",&retentionTime_) != 1)
      // {
      //   Rcpp::stop("Error: couldn't read retention_time!\n");
      // }
      continue;
    }
    else if (! strncmp(buffer,"RTINSECONDS=",12) ) // this is the official MGF field name
    {
      // if (sscanf(buffer+12,"%f",&retentionTime_) != 1)
      // {
      //   cout << "Error: couldn't read retention_time!" << endl;
      //   exit(1);
      // }
      continue;
    }
    // GOT NP3 peak profile and area
    else if (! strncmp(buffer,"RTMIN=",6) ) // this is the NP3 MGF field name
    {
      // if (sscanf(buffer+6,"%f",&retentionTimeMin_) != 1)
      // {
      //   cout << "Error: couldn't read retention_time_min!" << endl;
      //   exit(1);
      // }
      continue;
    }
    else if (! strncmp(buffer,"RTMAX=",6) ) // this is the Np3 MGF field name
    {
      // if (sscanf(buffer+6,"%f",&retentionTimeMax_) != 1)
      // {
      //   cout << "Error: couldn't read retention_time_max!" << endl;
      //   exit(1);
      // }
      continue;
    }
    else if (! strncmp(buffer,"PEAK_AREA=",10) ) // this is the Np3 MGF field name
    {
      // if (sscanf(buffer+10,"%f",&peakArea_) != 1)
      // {
      //   cout << "Error: couldn't read peak_area!" << endl;
      //   exit(1);
      // }
      continue;
    }
    else if (! strncmp(buffer,"PEAK_ID=",8) ) // this is the Np3 MGF field name
    {
      // int len = strlen(buffer)-1;
      // buffer[len]='\0';
      // if (buffer[len-1]=='\r' || buffer[len-1]=='\n' )
      //   buffer[len-1]='\0';
      // 
      // peakId_ = buffer + 8;
      //			if (sscanf(buffer+8,"%s",&peakId_) != 1)
      //			{
      //				cout << "Error: couldn't read peak_id!" << endl;
      //				exit(1);
      //			}
      continue;
    }
    else if (! strncmp(buffer,"CLUSTER_SIZE=",13) )
    {
      // if (sscanf(buffer+13,"%d",&clusterSize_) != 1)
      // {
      //   cout << "Error: couldn't read cluster size!" << endl;
      //   exit(1);
      // }
      // // NP3 GOT change clusterSize init when reading mgf
      // clusterSize_ = 1;
      continue;
    }
    else if (! strncmp(buffer,"PRECURSOR_INTENSITY=",20) )
    {
      // if (sscanf(buffer+20,"%f",&precursorIntensity_) != 1)
      // {
      //   cout << "Error: couldn't read cluster size!" << endl;
      //   exit(1);
      // }
      continue;
    }
    else if ( ! strncmp(buffer,"CHARGE=",7))
    {
      // int c;
      // if (sscanf(buffer,"CHARGE=%d",&c) != 1)
      // {
      //   cout <<  "Error: couldn't read charge!" << endl;
      //   return false;
      // }
      // charge_ = static_cast<short>(c);
      //cout << "@@@@@@@@@@@@@ CHARGE " << charge_ << endl;
      continue;
    }
    else if (! strncmp(buffer,"PEPMASS=",8))
    {
      //Rcpp::Rcout << "pepmass";
      std::istringstream is(buffer+8);
      is >> mOverZ;
      mz_prec.push_back(mOverZ);

      if (mOverZ < 0)
      {
        Rcpp::stop("Error: reading pepmass: ", mOverZ, "\n");
        return false;
      }
      continue;
    }
    else // is this a peak?
    {
      float mass = -1.0;
      float intensity = -1.0;
      
      // read all the peaks in the fragmentation list
      do
      {
        //Rcpp::Rcout <<  buffer << "\n";
        if (! strncmp(buffer,"END IONS",8) )
          break;
        
        std::istringstream is(buffer);
        is >> mass >> intensity;
        
        if (mass >0.0 && intensity>0.0)
        {
          mz.push_back(mass);
          inty.push_back(intensity);
        }
      } while ( fgets(buffer, 256, mgfStream) );
      // 
      // // append current peak list to the total list
      // mzs.push_back(mz);
      // ints.push_back(inty);
      // scans.push_back(scanNumber);
      // scanNumber = -1;
      // mz = std::vector<double>();
      // inty = std::vector<double>();
    }
  }
  fclose(mgfStream);
  
  return List::create(_["SCANS"] = (scans), 
                      _["MZS"] =  wrap(mzs), 
                      _["INTS"] = wrap(ints),
                      _["PREC_MZ"] = (mz_prec));
}

// if trim_mz == -1 do not trim spectra; if trim_mz != -1 trim spectra by precursor mz + 5*iso mass + bin_size
// scale_factor: 0 - ln; (0,] expoent of pow; and if -1 no join nor scale nor trim nor norm
// [[Rcpp::export]]
List readMgfHeader(std::string filePath) {
  FILE*  mgfStream = fopen(filePath.c_str(),"r");
  
  double ISO_MASS = 1.0033;
  std::vector<double> mz_prec;
  std::vector<float> ms2_int, peak_area, rt, rtmin, rtmax;
  std::vector<std::string> peak_id;
  std::vector<int> scans;
  
  int scanNumber = -1;
  double mOverZ = -1.0;
  std::string peakId_ = "null";
  float retentionTime_ = -1.0, retentionTimeMax_ = -1.0, retentionTimeMin_ = -1.0;
  float peakArea_ = -1.0, precursorIntensity_ = -1.0;
  
  char buffer[256];
  //assert(fileType_ == IFT_MGF);
  
  while (fgets(buffer, 256, mgfStream))
  {
    if (strncmp(buffer,"BEGIN IONS",10))
      continue;
    break;
  }
  
  long positionInFile = ftell(mgfStream);
  //float precursorIntensity_ = 0.0;
  //float firstPeakIntensity=0.0;
  
  if (positionInFile < 0) {
    Rcpp::stop("Bad skip position in mgf file! This can often be corrected by running unix2dos (or vice versa if appropriate)\n");
  }
  
  // read header info and first peak
  while (true)
  {
    if (! strncmp(buffer,"END IONS",7))
    {
      //Rcpp::Rcout << mOverZ << " \n";
      scans.push_back(scanNumber);
      peak_id.push_back(peakId_);
      rt.push_back(retentionTime_);
      rtmin.push_back(retentionTimeMin_);
      rtmax.push_back(retentionTimeMax_);
      ms2_int.push_back(precursorIntensity_);
      mz_prec.push_back(mOverZ);
      peak_area.push_back(peakArea_);
      
      // got to next ion
      while (fgets(buffer, 256, mgfStream))
      {
        if (strncmp(buffer,"BEGIN IONS",10))
          continue;
        break;
      }
    }
    // eof
    if( ! fgets(buffer, 256, mgfStream))
      break;
    
    //Rcpp::Rcout << buffer << " \n";
    
    if (!strncmp(buffer,"BEGIN IONS",10))
      continue;
    
    if (!strncmp(buffer,"TITLE=",6) || ! strncmp(buffer,"SEQ=",4) || 
        ! strncmp(buffer,"PEPSEQ=",7) || ! strncmp(buffer,"NUM_PEAKS=",10) || 
        ! strncmp(buffer,"SQS=",4) || ! strncmp(buffer,"CHARGE=",7))
    {
      continue;		
    }
    else if (! strncmp(buffer,"SCAN=",5) )
    {
      //Rcpp::Rcout << "scan";
      if (sscanf(buffer+5,"%d",&scanNumber) != 1)
      {
        Rcpp::stop("Error: couldn't read scan number from mgf file!\n");
      }
      continue;
    }
    else if (! strncmp(buffer,"SCANS=",6) ) // this is the offical MGF field, only the first number is kept
    {
      //Rcpp::Rcout << "scans";
      if (sscanf(buffer+6,"%d",&scanNumber) != 1)
      {
        Rcpp::stop("Error: couldn't read scan number from mgf file!\n");
      }
      continue;
    }
    else if (! strncmp(buffer,"RT=",3) )
    {
      if (sscanf(buffer+3,"%f",&retentionTime_) != 1)
      {
        Rcpp::stop("Error: couldn't read retention_time!\n");
      }
      continue;
    }
    else if (! strncmp(buffer,"RTINSECONDS=",12) ) // this is the official MGF field name
    {
      if (sscanf(buffer+12,"%f",&retentionTime_) != 1)
      {
        Rcpp::stop("Error: couldn't read retention_time!");
      }
      continue;
    }
    // GOT NP3 peak profile and area
    else if (! strncmp(buffer,"RTMIN=",6) ) // this is the NP3 MGF field name
    {
      if (sscanf(buffer+6,"%f",&retentionTimeMin_) != 1)
      {
        Rcpp::stop("Error: couldn't read retention_time_min!");
      }
      continue;
    }
    else if (! strncmp(buffer,"RTMAX=",6) ) // this is the Np3 MGF field name
    {
      if (sscanf(buffer+6,"%f",&retentionTimeMax_) != 1)
      {
        Rcpp::stop("Error: couldn't read retention_time_max!");
      }
      continue;
    }
    else if (! strncmp(buffer,"PEAK_AREA=",10) ) // this is the Np3 MGF field name
    {
      if (sscanf(buffer+10,"%f",&peakArea_) != 1)
      {
        Rcpp::stop("Error: couldn't read peak_area!");
      }
      continue;
    }
    else if (! strncmp(buffer,"PEAK_ID=",8) ) // this is the Np3 MGF field name
    {
      int len = strlen(buffer)-1;
      buffer[len]='\0';
      if (buffer[len-1]=='\r' || buffer[len-1]=='\n' )
        buffer[len-1]='\0';

      peakId_ = buffer + 8;
  		// if (sscanf(buffer+8,"%s",&peakId_) != 1)
  		// {
  		//   Rcpp::stop("Error: couldn't read peak_id!");
  		// }
      continue;
    }
    else if (! strncmp(buffer,"PRECURSOR_INTENSITY=",20) )
    {
      if (sscanf(buffer+20,"%f",&precursorIntensity_) != 1)
      {
        Rcpp::stop("Error: couldn't read cluster size!");
      }
      continue;
    }
    else if (! strncmp(buffer,"PEPMASS=",8))
    {
      //Rcpp::Rcout << "pepmass";
      std::istringstream is(buffer+8);
      is >> mOverZ;
      
      if (mOverZ < 0)
      {
        Rcpp::stop("Error: reading pepmass: ", mOverZ, "\n");
        return false;
      }
      continue;
    }
    else // is this a peak?
    {
      float mass = -1.0;
      float intensity = -1.0;
      
      // read all the peaks in the fragmentation list
      do
      {
        //Rcpp::Rcout <<  buffer << "\n";
        if (! strncmp(buffer,"END IONS",8) )
          break;
        
        std::istringstream is(buffer);
        is >> mass >> intensity;
      } while ( fgets(buffer, 256, mgfStream) );
    }
  }
  fclose(mgfStream);
  
  return DataFrame::create(_["scans"] = (scans), 
                      _["mz"] =  (mz_prec), 
                      _["int"] = (ms2_int),
                      _["rt"] = (rt),
                      _["rtmin"] = (rtmin),
                      _["rtmax"] = (rtmax),
                      _["peak_area"] = (peak_area),
                      _["peak_id"] = (peak_id),
                      _["stringsAsFactors"] = false);
}
