#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 14:49:56 2018
"""
import pandas as pd
import sys
#import time

#initTime = time.time()

#To not truncate larger string rows
pd.set_option('display.max_colwidth', -1)

csvFile = str(sys.argv[1])

try:
    areaBool = sys.argv[2]
    
    if areaBool.lower() == "false":
        areaBool = False
    
    if type(areaBool) == bool:
        pass
    else:
        try:
            areaBool = bool(areaBool)
        except:
            areaBool = True
        
except:
    areaBool = True
    

try:
    numRound = int(sys.argv[3])
    if type(numRound) == int:
        pass
    else:
        try:
            numRound = int(numRound)
        except:
            numRound = 3
except:
    numRound = 3

#print("File name: ", end = "")
#print(csvFile)
#print(type(csvFile))
#print("Is Area? ", end = "")
#print(areaBool)
#print(type(areaBool))
#print("Round by: ", end = "")
#print(numRound)
#print(type(numRound))

dfMSCluster = pd.read_csv(csvFile)
orderedCols = dfMSCluster.columns.tolist()


def listToMerge(dataFrame):
    '''
    This function looks for annotation that contains  "[M]2+ " or "[M]3+" and lists the msclusterID number
    of the lines that are ment do join
    
    Parameters
    dataFrame:
        Type: Pandas Data Frame
        description: The target Data Frame
    '''
    
    dfAnnot = dataFrame[dataFrame["annotation"].str.contains("\[M\]2\+",na=False) | dataFrame["annotation"].str.contains("\[M\]3\+",na=False) | dataFrame["annotation"].str.contains("\[2M\+H\]\+",na=False) | dataFrame["annotation"].str.contains("\[2M\+Na\]\+",na=False)]  

    #List of pairs to merge
    pairList = []
    
    #index list of rows to merge
    singleList = []
    
    #for each annotation gets the id to look for double charges
    for item in dfAnnot.annotation:
        for msid in list(dfMSCluster["msclusterID"][dfMSCluster.annotation==item]):
            templist =[]
            templist.append(msid)
            annotList = item.split(";")
        
        #Convert to index number
        ind = int(dataFrame[dataFrame["msclusterID"] == templist[0]].index[0])
        singleList.append(ind)
                
        for i in annotList:
            if ("[M]2+ " in i) or ("[M]3+ " in i):
               begin = i.find("[",5)
               end = i.find("]",5)
               otherID = int(i[begin+1:end])
            elif ("[2M+H]+ " in i) or ("[2M+Na]+ " in i):
               begin = i.find("[",10)
               end = i.find("]",10)
               otherID = int(i[begin+1:end])
            else:
                pass
                
        templist.append(otherID)
        
        
        #Conver to index number
        ind = int(dataFrame[dataFrame["msclusterID"] == otherID].index[0])
        singleList.append(ind)
        
        pairList.append(templist)
        
    return [pairList, singleList]





        
def ponderada( df, col, pond, arredonda = 3):
    '''
    Calculates the weighted average of certain column weighted by another column
    Parameters
    df:
        Type: Pandas Data Frame
        description: The target Data Frame
    col:
        Type: String
        description: The name of the target column
    pond:
        Type: String
        description: The name of the column that will weight the average
    arredonda:
        Type: integer
        description: How many digits to round by
    '''
    
    soma = 0
    somaPond = 0
    somaInt = 0
    i=0
    for ind in df.index:
        mz = df[col][ind]
        Ints = df[pond][ind]
        somaPond += mz*Ints
        soma += mz
        somaInt += Ints
        i += 1
    
    try:
        quocient = somaPond/somaInt
    except:
        quocient = soma/i
        
    return round(quocient, arredonda)
    



def mergeFrames(dataFrame, area = True):
    '''
    Merge the rows of a Data Frame with the following detetails:
    
    msclusterID column:
        get the maximum value
    
    mzConsensus column:
        get the average that can be weighted by peak area or number of spectra
    
    rtMean column:
        get the average that can be weighted by peak area or number of spectra

    rtMin column:
        get the average that can be weighted by peak area or number of spectra
        
    rtMax column:
        get the average that can be weighted by peak area or number of spectra
    
    sumInts column:
        Sum all values
    
    numJoins column:
        Sum all values and add 1 on final result
        
    annotation column:
        #For annotation gets the non empty row and adds the ids of the joined rows
    
    FLAGs columns:
        Flags columns are boolean data, it sum the column values and make sure that the output keeps boolean
        
    Other columns:
        Sums the UNIQUE values of the columns
        
    Parameters
    
    '''
    
    
    #Special cases that will note necessaraly get added
    specials = ["msclusterID", "mzConsensus", "rtMean", "rtMin", "rtMax", "sumInts", "numJoins", "annotation", "BFLAG", "CFLAG","HFLAG","DESREPLICATION","UNPD_IDs_tremolo", "SMILES_tremolo", "chemicalNames_tremolo", "MQScore_tremolo", "mzErrorPPM_tremolo", "numSharedPeaks_tremolo","CAS_tremolo","molecularFormula_tremolo"]
    textCols = ["annotation", "UNPD_IDs_tremolo", "SMILES_tremolo", "chemicalNames_tremolo", "MQScore_tremolo", "mzErrorPPM_tremolo", "numSharedPeaks_tremolo","HFLAG","DESREPLICATION","CAS_tremolo","molecularFormula_tremolo"]

    #Dict to create the dataFrame with the new values
    dictFrame = {}
    
    #Dimer or DoubleCharge
    
    if len(list(dataFrame["msclusterID"][dataFrame["annotation"].str.contains("\[2M\+",na=False)])) > 0:
        Dimer = True
    else:
        Dimer = False
    
    
    #Lets read the cols of the dataframe
    for coluna in dataFrame.columns:
        if coluna in specials:
            #For msclusterID, lets get the bigger value
            if coluna == "msclusterID":
                if Dimer:
                    dictFrame[coluna] = dataFrame[coluna].min()
                else:
                    dictFrame[coluna] = dataFrame[coluna].max()
            #For mzConsensus get the average that can be weighted by peak area or number of spectra
            elif coluna == "mzConsensus":
                if Dimer:
                    dictFrame[coluna] = dataFrame[coluna].min()
                else:
                    dictFrame[coluna] = dataFrame[coluna].max()
            #For rtMean get the average that can be weighted by peak area or number of spectra
            elif coluna == "rtMean":
                if area:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "sumInts", numRound)
                else:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "numSpectra", numRound)
            #For rtMin get the average that can be weighted by peak area or number of spectra
            elif coluna == "rtMin":
                if area:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "sumInts", numRound)
                else:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "numSpectra", numRound)
            #For rtMax get the average that can be weighted by peak area or number of spectra
            elif coluna == "rtMax":            
                if area:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "sumInts", numRound)
                else:
                    dictFrame[coluna] = ponderada(dataFrame, coluna, "numSpectra", numRound)
            #For sumInts column sum all values
            elif coluna == "sumInts":
                dictFrame[coluna] = dataFrame[coluna].sum()
            #For numJoins sum all values and add 1 on final result
            elif coluna == "numJoins":
                nj = dataFrame[coluna].sum()
                nj += dataFrame.shape[0]-1
                dictFrame[coluna] = nj
            #For annotation gets the non empty row and adds the ids of the joined rows
            elif coluna in textCols:
                if coluna.strip() in ["HFLAG","DESREPLICATION"]:
                    note = dataFrame[coluna][dataFrame[coluna].isna()==False].to_string(index = False)
                    
                    if note == "Series([], )":
                        note = ""

                    noteList = note.split(";")
                    #noteList = list(set(noteList))
                    
                    newNote = []
                    for n in noteList:
                        if not (n in newNote):
                            newNote.append(n)
                    
                    
                    note = ""
                    for n in newNote:
                        note += str(n)+";"
                    
                    note = note[:len(note)-1]
                    dictFrame[coluna] = note
                    
                
                elif coluna != "annotation":
                    note = dataFrame[coluna][dataFrame[coluna].isna()==False].to_string(index = False)
                    
                    if note == "Series([], )":
                        note = ""
                    
                
                
                else:
                    note = dataFrame[coluna][dataFrame[coluna].isna()==False].to_string(index = False)                    
                    if note == "Series([], )":
                        note = ""                    
                    note += "; Joined ["
                    for Id in dataFrame.msclusterID:
                        note += str(Id)+", "
                        
                    note = note.strip()
                    note = note[:len(note)-1]
                    note += "]"
                
                dictFrame[coluna] = note
            #Flags columns are boolean data it sum the column values and make sure that the output keeps boolean
            if ((coluna == "BFLAG") or (coluna == "CFLAG")):
                try:
                    dictFrame[coluna] = bool(dataFrame[coluna].sum())
                except:
                    dictFrame[coluna] = dataFrame[coluna].sum()
            
        #Other columns
        else:
            #If har  COR_ in the header name gets, the max value
            if "COR_" in coluna:
                dictFrame[coluna] = dataFrame[coluna].max()
            #Else sum the lines
            elif coluna == "numSpectra":
                dictFrame[coluna] = dataFrame[coluna].sum()
            else:
                if area:
                    uvalues = dataFrame[coluna].unique()
                    dictFrame[coluna] = uvalues.sum()
                else:
                    dictFrame[coluna] = dataFrame[coluna].sum()
    
    #Returns the merged data frames
    return pd.DataFrame(dictFrame, index=[dictFrame["msclusterID"]])

#Sum of the unique values, if this function is needed
def SomUnique(dataFrame, colName):
    uvalues = dataFrame[colName].unique()
    uSum = uvalues.sum()
    
    return uSum

#para testes
#a = listToMerge(dfMSCluster)[0]
#b=dfMSCluster[dfMSCluster["msclusterID"].isin(a[27])]
#c = mergeFrames(b)




#Now is time to join these double charges, so let's do it step by step.

'''
Step 1)
    Look for the double charges using the listToMerge function
'''
mergeLists = listToMerge(dfMSCluster)
toJoin = mergeLists[0]

#Verify if there are more than one source pointing to the same target
source = []
target = []
for i in toJoin:
    source.append(i[0])
    target.append(i[1])

poinList = pd.DataFrame({"Source":source,"Target":target})

#Count howmany times the target is pointed
countTarget = poinList.groupby(["Target"]).count()

#Reset toJoin list
toJoin = []

#Saving pairs
for tar in countTarget.index[countTarget.Source==1]:
    fstCoord = int(poinList["Source"][poinList.Target==tar])
    toJoin.append([fstCoord, tar])

#If we have more than pair

auxList = []
for tar in countTarget.index[countTarget.Source>1]:
    for i in poinList["Source"][poinList.Target==tar]:
        auxList.append(i)
        
    auxList.append(tar)
    
    toJoin.append(auxList)
    
    auxList=[]




'''
Step 2)
    Lets sum the rows we found on step (1)
'''
#list to keep the df's with joined rows
dfList = []

#This loop append the sums at dfList
for pair in toJoin:
    tFrame = dfMSCluster[dfMSCluster["msclusterID"].isin(pair)]
    tFrame = mergeFrames(dataFrame = tFrame, area = areaBool)
    dfList.append(tFrame)

'''
Step 3)
    Get all index number of the joined rows and drop all these rows of the dataframe
'''

toDrop = mergeLists[1]
toDrop.sort()
newMSCluster = dfMSCluster.drop(toDrop)


''' 
Step 4) 
    Incorporate and all merged lines on the dataframe an sort it by msclusterID
'''    


for row in dfList:
    newMSCluster = newMSCluster.append(row)
        
newMSCluster = newMSCluster.sort_values(by=["msclusterID"])

uniqueIDList = list(newMSCluster["msclusterID"].unique())

newMSCluster = newMSCluster[newMSCluster["msclusterID"].isin(uniqueIDList)]
newMSCluster = newMSCluster.sort_values(by=["msclusterID"])

newName = csvFile[:len(csvFile)-4]


newName +="_merged_doublecharge.csv"
newMSCluster.to_csv(newName,index = False, columns=orderedCols)

#finalTime = time.time()
#DeltaT = finalTime - initTime
#print("Tempo de execução: "+str(round(DeltaT,3))+" segundos")
