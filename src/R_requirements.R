depend_pcks <- c('purrr', 'dplyr', 'stats', 'inline', 'rJava', 'devtools', 'readr', 'Rcpp', 'RcppArmadillo',
                 "gtools", 'BiocManager')
bio_pcks <- c('MSnbase', 'xcms')

if (length(setdiff( depend_pcks, rownames(installed.packages()))) > 0)  
{ 
  message('\nInstalling R packages: ', 
          paste(setdiff(depend_pcks, rownames(installed.packages())), 
                collapse = ", "), '\n')
  install.packages(setdiff(depend_pcks, 
                           rownames(installed.packages())), repos = 'https://cloud.r-project.org/', dependencies = TRUE) 
}

# metfrag
if (length(setdiff(c("rJava", "devtools", "metfRag"), rownames(installed.packages()))) > 0 && 
    all(setdiff(c("rJava", "devtools", "metfRag"), rownames(installed.packages())) == "metfRag"))
{
  require("rJava")
  require("devtools") 
  install_github("c-ruttkies/MetFragR/metfRag")
}

if (length(setdiff(bio_pcks, rownames(installed.packages()))) > 0)
{
  # upgrade BiocManager and install dependencies
  BiocManager::install() 
  message('\nInstalling R package: ', 
          paste(bio_pcks[!(bio_pcks %in% rownames(installed.packages()))], collapse = ", "), '\n')
  BiocManager::install(bio_pcks[!(bio_pcks %in% rownames(installed.packages()))], update = TRUE)
}

# check if the packages were installed
if (length(setdiff(c(bio_pcks, depend_pcks), rownames(installed.packages()))) > 0) 
{
  stop("The following R dependencies could not be installed: ", 
       paste(setdiff(c(bio_pcks, depend_pcks), rownames(installed.packages())), collapse = ", "),
       ". Look for ERROR messages, install the missing libraries and retry.")
}

# check if the packages can be loaded
required_pcks <- sapply(c(bio_pcks, depend_pcks), 
                        function(x) 
                          suppressPackageStartupMessages(
                            require(x ,character.only = TRUE)))
if (any(!required_pcks)) {
  stop("The following R dependencies could not be loaded: ", 
       paste(c(bio_pcks, depend_pcks)[!required_pcks], collapse = ", "),
       ". Look for ERROR messages, install the missing libraries and retry.")
}