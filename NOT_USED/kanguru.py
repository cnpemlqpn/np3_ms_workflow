#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 11:03:21 2018

@author: luizim
"""

from pyteomics import mgf
import numpy as np
from operator import itemgetter


def creatematrix(): # Header of the matrix | feature_id | pepmass | rt | MS2 | Intensity |
    filename = "PB181_mzmine_for_gnpsA.mgf"
    MSdata = [] #Matrix that will keep MS data from mgf file
    auxvect =[] #auxiliar vector for MSdata indexing
    with mgf.read(filename) as reader:
        for data in reader:
            for i in range(len(data["m/z array"])):
                auxvect.append(int(data["params"]["feature_id"])) #get feature_id column
                auxvect.append(data["params"]["pepmass"][0]) #get pepmass column
                auxvect.append(float(data["params"]["rtinseconds"])) #get rt column
                auxvect.append(data["m/z array"][i]) #get MS2 column
                auxvect.append(data["intensity array"][i]) #get intensity column
                MSdata.append(auxvect[:]) #Append to the main array
                auxvect.clear()
    ms = np.array(MSdata)
    return(ms)

MSdata = creatematrix()
print("MS2 Matrix - Done")

RTorderned = np.array(sorted(MSdata[:], key=itemgetter(2))) #Order the matrix by RT 
print("RT ordenation - Done")

def gapclass(rtordermtx): # create classes using rt diference
    colsize = rtordermtx.shape[0]
    classe = 0
    classcol = []
    deltaT = 9
    for i in range(colsize):
        if (i == colsize) and (abs(rtordermtx[i][2]-rtordermtx[i-1][2]) < deltaT):
            classcol.append(classe)
        if (i == colsize) and (abs(rtordermtx[i][2]-rtordermtx[i-1][2]) >= deltaT):
            classcol.append(classe)
            classe += 1
        if (i + 1 <colsize) and (abs(rtordermtx[i][2]-rtordermtx[i+1][2]) < deltaT):
            classcol.append(classe)
        else:
            classcol.append(classe)
            classe += 1
    return classcol

a = np.array(gapclass(RTorderned))

#Let's add the column a to RTordened
def addcol(matrix, col):
    auxvect = []
    auxmtx = []
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            auxvect.append(matrix[i][j])
        auxvect.append(col[i])
        auxmtx.append(auxvect[:])
        auxvect.clear()
    return auxmtx
    
Orderedclass = np.array(addcol(RTorderned,a))