# !!! THIS REPOSITORY IS DEPRECATED !!!
# PLEASE GO TO: https://github.com/danielatrivella/NP3_MS_Workflow

# NP³ MS Workflow

## A Pipeline for LC-MS/MS Metabolomics Data Process and Analysis  


This repository contains the NP³ MS workflow version used in the "Chemical Elicitors Induce Rare Bioactive Secondary Metabolites in Deep-Sea Bacteria under Laboratory Conditions" paper (https://www.mdpi.com/2218-1989/11/2/107/html). This version is not supported anymore and this repository have being migrated to github.


For the new version of the NP³ MS workflow please go to the following repository: https://github.com/danielatrivella/NP3_MS_Workflow

