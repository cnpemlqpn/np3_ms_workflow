#!/usr/bin/env node

/**
 * Module dependencies.
 */

var shell = require('shelljs');
shell.config.silent = true;
var program = require('commander');
//var child_process = require('child_process')
var test_res = [];

/**
 *  Auxiliary functions
 */

/**
 * @param t process.hrtim(t_initial) - a time diff
 * @returns {string}
 */
function printTimeElapsed(t) {
  return ('Time Elapsed: ' + (t[0]/60).toFixed(0) + "min " +
      (t[0]%60).toFixed(0) + "s " + (t[1]/1000000).toFixed(2) + "ms") ;
}

function parseDecimal(val) {
  return parseInt(val, 10);
}

function splitListFloat(val) {
  var floatList = val.split(",").map(parseFloat);

  if (floatList.some(isNaN) || floatList.length < 2)
  {
    console.error('\nERROR. Wrong rt_tolerance parameter value \'' + val+
      '\'. The retention time tolerance must be two numeric values separated by a comma (e.g. \'1,2\'). Execution aborted.');
    process.exit(1);
  }

  return floatList;
}

function splitList(val) {
  return val.split(",");
}

function splitAnnotationsInitials(anns) {
  var anns_initials = "";
  anns.split(",").forEach(function (ann) {
    anns_initials += ann.charAt(0).toLowerCase();
  });
  return anns_initials;
}

function increaseVerbosity(v, total) {
  return total + 1;
}

function basename(str, sep) {
  if (str[str.length - 1] === sep) {
    str = str.substring(0, str.length - 1);
  }

  return str.substr(str.lastIndexOf(sep) + 1);
}

function convertIonMode(mode) {
  mode = parseDecimal(mode);

  if (![1,2].includes(mode)) {
    console.error('\nERROR. Wrong ion_mode parameter value. The ion mode must be a positive numeric value in {1,2}. Execution aborted.');
    process.exit(1);
  }

  if (mode === 2)
    return(-1);
  else
    return(mode);
}

function convertMethodCorr(method) {
  //method = parseDecimal(method);
  //console.log(method);

  if (!["pearson", "kendall", "spearman"].includes(method)) {
    console.error('\nERROR. Wrong method parameter value. The correlation method must be one of {"pearson", "kendall", "spearman"}. Execution aborted.');
    process.exit(1);
  }

  return(method);
}

function callClustering(options, output_path, specs_path) {
  var start_clust = process.hrtime();
  // copy MSCluster model
  //shell.cp(options.model_dir+"/LTQ_TRYP_config.txt", output_path);
  // copy metadata table
  shell.cp(options.metadata, output_path);
  processed_dir = options.raw_data_path+osSep()+options.processed_data_name;

  // call not blank samples clustering, SAMPLE_TYPE != blank
  if (shell.test('-e', specs_path+"/data_lists") && !(shell.ls("-A", specs_path+"/data_lists").toString() === ""))
  {
    shell.ls("-A", specs_path+"/data_lists").forEach(function (spec) {

      var out_name = spec.toString().split(".")[0];
      shell.mkdir("-p", output_path+"/outs/"+out_name);
      callMSCluster(options, options.similarity,specs_path+"/data_lists/"+spec.toString(),
          out_name, output_path, 0, false, 1, 2);
      callCountSpectraBySample(output_path+"/outs/"+out_name, out_name, options.metadata,
          0, options.mz_tolerance, options.verbose);
    });
  }

  // call blank samples clustering, SAMPLE_TYPE == blank
  if (shell.test('-e', specs_path+"/blank_lists") && !(shell.ls("-A", specs_path+"/blank_lists").toString() === ""))
  {
    shell.ls("-A", specs_path+"/blank_lists").forEach(function (spec) {

      var out_name = spec.toString().split(".")[0];
      shell.mkdir("-p", output_path+"/outs/"+out_name);
      callMSCluster(options, options.similarity_blank, specs_path+"/blank_lists/"+spec.toString(),
          out_name, output_path, 0, false, 1, 2);
      callCountSpectraBySample(output_path+"/outs/"+out_name, out_name, options.metadata,
          0, options.mz_tolerance, options.verbose);
    });
  }

  // call data collection integration step clustering
  if (shell.test('-e', specs_path+"/batch_lists") && !(shell.ls("-A", specs_path+"/batch_lists").toString() === ""))
  {
    shell.ls("-A", specs_path+"/batch_lists").forEach(function (spec) {

      var out_name = spec.toString().split(".")[0];
      shell.mkdir("-p", output_path+"/outs/"+out_name);
      callMSCluster(options, options.similarity,specs_path+"/batch_lists/"+spec.toString(),
          out_name, output_path, 0, false, 1, 2);

      if (shell.test("-e", output_path+'/outs/'+out_name+'_0') || shell.test("-e", output_path+'/outs/'+out_name+'_1'))
      {
        callCountSpectraBySubClusterID(output_path+"/outs/", out_name, 0, options.metadata,
            processed_dir, options.mz_tolerance,  options.verbose);
      } else {
        callCountSpectraBySample(output_path+"/outs/"+out_name, out_name, options.metadata,
            0, options.mz_tolerance, options.verbose);
      }
    });
  }

  console.log('*** Integrating batches ***\n');
  shell.mkdir("-p", output_path+"/outs/"+options.output_name);
  callMSCluster(options, options.similarity,specs_path+'/out_spec_lists.txt', options.output_name, output_path,
      1, true, options.min_peaks_output, 1);

  if (shell.test("-e", output_path+'/outs/B_1') || shell.test("-e", output_path+'/outs/B_1_0') || shell.test("-e", output_path+'/outs/B_1_1'))
  {
    callCountSpectraBySubClusterID(output_path+"/outs/", options.output_name, 1,
        options.metadata, processed_dir, options.mz_tolerance, options.verbose);
  } else {
    callCountSpectraBySample(output_path+"/outs/"+options.output_name, options.output_name,
        options.metadata, 1, options.mz_tolerance, options.verbose);
  }

  console.log('DONE clustering and counting spectra!\n' + printTimeElapsed(process.hrtime(start_clust))+ "\n");

  // for analysing the clustering counts
  callAnalyseCount(output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_spectra.csv",
      output_path+"/outs/"+options.output_name+"/count_tables/analyseCountClustering");

  // concatenate spectra peak list
  callExtractPeakList(options.output_name, output_path+"/outs/"+options.output_name+"/mgf/",
      output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_peak_area.csv",
      output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_spectra.csv",
      options.fragment_tolerance, options.scale_factor);

  // call aggregation of not fragmented MS1 peaks
  // if exists options.raw_data_path+osSep()+options.processed_data_name+osSep()+"mzs_no_MS2.csv"
  if (shell.test('-e', options.raw_data_path+osSep()+options.processed_data_name+osSep()+"mzs_no_MS2.csv"))
    callCleanNoMs2Counts(options.raw_data_path+osSep()+options.processed_data_name+osSep()+"mzs_no_MS2.csv" ,
        options.metadata, output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_peak_area_MS1.csv",
        options.mz_tolerance, options.rt_tolerance[1], options.method, options.verbose)

}


function callMSCluster(parms, sim_tol, spec, name, out_path, rt_tol_i, keep_split_mgf, min_numPeak_output = 1,
                       min_verbose) {
  //shell.cd('NP3_MSCluster');
  console.log('*** Calling MSCluster for: '+name+' *** \n');
  var resExec;

  if (isWindows())
  {
    resExec = shell.exec('NP3_MSCluster/NP3_MSCluster_bin.exe --list '+spec+' --output-name '+name+' ' +
        '--out-dir '+out_path+'\\outs\\'+name+' --rt-tolerance '+parms.rt_tolerance[rt_tol_i]+
      ' --fragment-tolerance '+parms.fragment_tolerance+' --window '+parms.mz_tolerance+' --similarity '+sim_tol+
      ' --model-dir '+parms.model_dir+' --sqs 0.0 --num-rounds '+parms.num_rounds+' --mixture-prob '+parms.mixture_prob+
      ' --tmp-dir NP3_MSCluster/tmp_'+parms.output_name+'_'+name+' --min-peaks-output '+min_numPeak_output+
        ' --scale-factor '+parms.scale_factor +' --verbose-level 10 --output-mgf --assign-charges --major-increment 100 ' +
        '--output-file-size '+parms.max_chunk_spectra, {async:false, silent:(parms.verbose < min_verbose)});
  } else {
    resExec = shell.exec('./NP3_MSCluster/NP3_MSCluster_bin --list '+spec+' --output-name '+name+' ' +
        '--out-dir '+out_path+'/outs/'+name+' --rt-tolerance '+parms.rt_tolerance[rt_tol_i]+
      ' --fragment-tolerance '+parms.fragment_tolerance+' --window '+parms.mz_tolerance+' --similarity '+sim_tol+
      ' --model-dir '+parms.model_dir+' --sqs 0.0 --num-rounds '+parms.num_rounds+' --mixture-prob '+parms.mixture_prob+
      ' --tmp-dir ./NP3_MSCluster/tmp_'+parms.output_name+'_'+name+' --min-peaks-output '+min_numPeak_output+
        ' --scale-factor '+parms.scale_factor +' --verbose-level 10 --output-mgf --assign-charges --major-increment 100 ' +
        '--output-file-size '+parms.max_chunk_spectra, {async:false, silent:(parms.verbose < min_verbose)}); //  --assign-charges
  }
  // save mscluster log file
  shell.ShellString(resExec.stdout).to(out_path+'/outs/'+name+'/logRun');

  // remove tmp file and leave mscluster dir
  shell.rm('-rf', 'NP3_MSCluster/tmp_'+parms.output_name+'_'+name);
  shell.rm('-rf', 'NP3_MSCluster/out*');
  //shell.cd('..');

  if (resExec.code) {
    shell.cd('..');
    if (parms.verbose < min_verbose) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('ERROR');
    process.exit(1);
  } else {
    console.log('DONE!\n');
    // merge mgfs
    shell.cat(shell.cat(out_path+'/outs/'+name+'/'+name+'_0_0_mgf_list.txt').split("\n").filter(String)).to(
        out_path+'/outs/'+name+'/mgf/'+name+'_all.mgf');

    // rm unmerged mgfs when they wont be needed (except last step)
    if (!keep_split_mgf) {
      shell.rm('-rf', out_path + '/outs/' + name + '/mgf/*_[0-9].mgf');
    }
    shell.rm('-rf', out_path+'/outs/'+name+'/'+name+'_0_0_clust_list.txt');
    shell.rm('-rf', out_path+'/outs/'+name+'/'+name+'_0_0_mgf_list.txt');
  }
}

function callCountSpectraBySample(out_path, name, metadata, isFinal, mz_tol, verbose)
{
  console.log('*** Calling R script to count peak area and spectra by sample '+name+' *** \n');
  var resExec = shell.exec('Rscript src/count_clust_samples.R '+out_path+' '+name+' '+metadata+' '+isFinal+' '+mz_tol,
    {async:false, silent:(verbose <= 0)});

  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR');
    process.exit(1);
  } else {
    console.log('\nDONE!\n');
  }
}

function callCountSpectraBySubClusterID(out_path, name, isFinal, metadata, processed_dir, mz_tol, verbose)
{
  console.log('*** Calling R script to count peak area and spectra by batch subclusters '+name+' *** \n');
  var resExec = shell.exec('Rscript src/count_clust_batches.R '+out_path+' '+name+' '+isFinal+' '+metadata+' '+
      processed_dir+' '+mz_tol, {async:false, silent:(verbose <= 0)});

  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR');
    process.exit(1);
  } else {
    console.log('\nDONE!\n');
  }
}

function callComputeCorrelation(metadata, counts, method, bio_cutoff, verbose)
{
  //# params:
  //   #$1 - Path to the CSV batch metadata file containing filenames, sample codes, data collection batches and blanks;
  //   #$2 - Path to the CSV spectra count file;
  //   #$3 - Path to the batches output folder;
  //   #$4 - output name name;
  //   #$5 - Correlation method, one of: 'pearson' (default), 'kendall', or 'spearman'.
  //   # return NA when div/0, all samples = 0 or return 1.1 when sd equals zero
  console.log('*** Calling R script to compute the correlation between the counts and the samples bioactivity from '+
      basename(counts, osSep())+' *** \n');
  var resExec = shell.exec('Rscript src/bioactivity_correlation.R '+metadata+' '+counts+' '+method+' '+bio_cutoff,
    {async:false, silent:(verbose <= 0)});


  if (resExec.code) {
    // in case of error show all the emmited msgs
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('ERROR\n');
  } else {
    console.log('\nDONE!\n');
  }
}

function callAnalyseCount(counts, out_path)
{
  console.log('*** Calling R script to analyse the total count of spectra  *** \n');
  var resExec = shell.exec('Rscript src/analyse_count.R '+counts, {async:false, silent:false});

  if (resExec.code) {
    console.log(resExec.stdout);
    //console.log(resExec.stderr);
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');

    // save analyse count file
    shell.ShellString(resExec.stdout).to(out_path);
  }
}

function callExtractPeakList(job_name, mgf_dir, counts_area, counts_spectra, bin_size, scale_factor)
{
  console.log('*** Calling R script to extract the fragmented peaks list from the clustered mgf and concatenate to the counts file *** \n');
  var resExec = shell.exec('Rscript src/extract_peak_list.R '+job_name+' '+mgf_dir+' '+counts_area+' '+bin_size+' '+
      scale_factor+' '+counts_spectra, {async:false});

  if (resExec.code) {
    console.log(resExec.stdout);
    //console.log(resExec.stderr);
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');
  }
}

// output_path file name with path to save the result
function callCleanNoMs2Counts(quantification_table_path, metadata_path, output_path, mz_tol, rt_tol, method, verbose)
{
  console.log('*** Calling R script to clean the count of not fragmented MS1 peaks  *** \n');
  var resExec = shell.exec('Rscript src/clean_no_MS2_quantification.R  '+metadata_path+' '+output_path+' '+
      quantification_table_path+' '+mz_tol+' '+rt_tol, {async:false, silent:(verbose === 0)});

  if (resExec.code) {
    console.log(resExec.stdout);
    //console.log(resExec.stderr);
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');

    // call correlation
    callComputeCorrelation(metadata_path, output_path,
        method, 0, verbose);
  }
}

function callMergeCounts(output_path, output_name, processed_dir, metadata_path, method, verbose)
{
  console.log('*** Calling R script to merge the counts based in the provided annotations *** \n');
  annotations_merge = "isotopes,adducts,dimers,multicharges,fragments";

  var resExec = shell.exec('Rscript src/merge_annotation_counts.R '+output_path+' '+annotations_merge+' '+
      metadata_path+' '+processed_dir, {async:false, silent:(verbose === 0)});

  if (resExec.code) {
    if (verbose === 0) {
      console.log(resExec.stdout);
     // console.log(resExec.stderr);
    }
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');

    // run corr if the metadata was provided and at least one symbolic cluster was created
    if (typeof metadata_path != "undefined" && shell.test('-e',output_path + "/count_tables/merge/" +
        output_name + '_peak_area_merged_' + splitAnnotationsInitials(annotations_merge) + ".csv")) {
      // call correlation
      callComputeCorrelation(metadata_path, output_path + "/count_tables/merge/" +
          output_name + '_peak_area_merged_' + splitAnnotationsInitials(annotations_merge) + ".csv",
          method, 0, verbose);
      // call correlation
      callComputeCorrelation(metadata_path, output_path + "/count_tables/merge/" +
          output_name + '_spectra_merged_' + splitAnnotationsInitials(annotations_merge) + ".csv",
          method, 0, verbose);
    }
  }
}

function callCleanAnnotateClustering(parms, output_path, mz_tol, rt_tol, bin_size, processed_dir = "")
{
  // console.log('outp '+ output_path+ " rt "+rt_tol+" bin "+ bin_size)
  console.log('*** Calling R script to clean the clustering counts and to annotate spectra *** \n');
  if (processed_dir === "") {
    processed_dir = parms.raw_data_path + osSep() + parms.processed_data_name;
  }

    var resExec = shell.exec('Rscript src/clean_annotate_spectra_quantification.R '+parms.metadata+' '+output_path+' '+
        processed_dir+' '+mz_tol+' '+ parms.similarity+' '+ rt_tol+' '+bin_size+' '+parms.rules+' '+parms.ion_mode+' '+
        parms.scale_factor+' '+parms.max_chunk_spectra, {async:false, silent:(parms.verbose === 0)});

  if (resExec.code) {
    if (parms.verbose === 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');
  }

  return(resExec.code);
}

function callPairwiseComparision(out_name, out_path, mgf_path, bin_size, scaling_method, trim_mz, cores_parallel, verbose)
{
  console.log('*** Calling R script to compute the pairwise similarity comparisions of the resulting cluster consensus *** \n');
  var resExec = shell.exec('Rscript src/pairwise_similarity.R '+out_name+' '+mgf_path+' '+out_path+' '+bin_size+' '+
      scaling_method+' '+trim_mz+' '+cores_parallel, {async:false, silent:(verbose <= 0)});

  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR');
    process.exit(1);
  } else {
    console.log('\nDONE!\n');
  }

  return(resExec.code)
}

function callCreatMN(out_path, sim_mn, net_top_k, max_chunk_spectra, verbose)
{
  console.log('*** Calling R script to create the result Molecular Networking  *** \n');
  var resExec = shell.exec('Rscript src/molecular_networking.R '+out_path+' '+sim_mn+' '+net_top_k+' '+max_chunk_spectra,
      {async:false, silent:(verbose <= 0)});

  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR');
  } else {
    console.log('\nDONE!\n');
  }

  return(resExec.code)
}

function callProcessData(job, metadata, raw_dir, parms, verbose)
{
  var resExec;
  console.log('*** Calling R script to pre process the raw data and extract peak info *** \n');

  if (parms.fragment_tolerance) // called from the run cmd, auto process
  {
    resExec = shell.exec('Rscript src/tandem_peak_info_align.R ' + job + ' ' + metadata + ' ' + raw_dir+ ' ' +parms.rt_tolerance[1]+ ' ' +
      parms.fragment_tolerance + ' ' + parms.ppm_tolerance + ' ' + parms.ion_mode+' '+ parms.processed_data_name+' '+
        parms.processed_data_overwrite, {async: false, silent: (verbose <= 0)});
  } else { // called from the process cmd
    resExec = shell.exec('Rscript src/tandem_peak_info_align.R ' + job + ' ' + metadata + ' ' + raw_dir + ' ' +parms.rt_tolerance+ ' ' +
      parms.mz_tolerance+ ' ' + parms.ppm_tolerance + ' ' + parms.ion_mode+' '+ parms.processed_data_name +' '+
      parms.processed_data_overwrite+ ' ' +parms.peak_width+ ' ' +parms.snthresh+ ' ' +parms.pre_filter+ ' ' +
      parms.min_fraction+ ' ' +parms.bw+ ' '  +parms.bin_size + ' ' +parms.max_features+ ' ' +parms.noise+ ' ' +parms.mz_center_fun+
      ' '+ parms.max_samples_batch_align+' '+ parms.integrate_method+' '+ parms.fit_gauss, {async: false, silent: (verbose <= 0)});
  }

  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log("\nError msg logged to: " + raw_dir+'/logPreProcessError');
    console.log('\nERROR');
    // save log error
    shell.ShellString("ERROR\n\n"+resExec.stderr).to(raw_dir+'/logPreProcessError');
    process.exit(1);
  } else {
    console.log('\nDONE!\n');
  }
}

function tremoloIdentification(output_name, output_path, mgf, mz_tol, sim_tol, top_k, verbose, verbose_search) {
  var start = process.hrtime();
  console.log('*** The tremolo spectral library identification *** \n');

  // check if output_path exists
  if (!shell.test('-e', output_path))
  {
    shell.mkdir("-p", output_path);
  }

  // Give the path to the CSV file containing the description of the database
  var db_desc = "src/ISDB_tremolo_NP3/Data/dbs/UNPD_DB.csv";

  console.log(' Converting the mgf file to a pklbin file \n');
  // run the mgf file converter to pklbin
  var resExec = shell.exec('src/ISDB_tremolo_NP3/Data/tremolo/convert '+ mgf+' src/ISDB_tremolo_NP3/Data/results/spectra_mgf_'+
    output_name+'.pklbin', {async:false, silent: (verbose <= 0)});

  if (!resExec.code) { // error code is 0
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR. Could not convert the spectra mgf to a pklbin file.\n');
    return(resExec.code);
  } else {
    console.log("DONE!\n");
  }

  shell.ShellString("EXISTING_LIBRARY_MGF=src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p01.mgf src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p02.mgf " +
    "src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p03.mgf src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p04.mgf " +
    "src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p05.mgf src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p06.mgf src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p07.mgf " +
    "src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p08.mgf src/ISDB_tremolo_NP3/Data/dbs/UNPD_ISDB_R_p09.mgf\n\n" +
    "searchspectra=src/ISDB_tremolo_NP3/Data/results/spectra_mgf_"+output_name+".pklbin\n\n" +
    "RESULTS_DIR=src/ISDB_tremolo_NP3/Data/results/Results_tremolo_"+output_name+".out\n\n" +
    "tolerance.PM_tolerance="+mz_tol+"\n\n" +
    "search_decoy=0\n\n" +
    "SCORE_THRESHOLD="+sim_tol+"\n\n" +
    "TOP_K_RESULTS="+top_k+"\n\n" +
    "NODEIDX=0\n" +
    "NODECOUNT=1\n\n" +
    "SEARCHABUNDANCE=0\n" +
    "SLGFLOADMODE=1").to('src/ISDB_tremolo_NP3/Data/results/scripted_'+output_name+'.params');

  console.log(' Running the tremolo search \n');

  // run the tremolo search
  resExec = shell.exec('src/ISDB_tremolo_NP3/Data/tremolo/main_execmodule ExecSpectralLibrarySearch ' +
    'src/ISDB_tremolo_NP3/Data/results/scripted_'+output_name+'.params ', {async:false, silent: (verbose_search === 0)});
  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('\nERROR. Couldn\'t run the spectral library search.\n');
    return(resExec.code);
  } else {
    console.log("DONE!\n");
    shell.ShellString(resExec.stdout).to(output_path+"/logTremolo")
  }

  resExec = shell.exec('python3 src/ISDB_tremolo_NP3/Data/dbs/treat.py src/ISDB_tremolo_NP3/Data/results/Results_tremolo_' +
      output_name+'.out ' +output_path + ' ' +db_desc,
      {async:false, silent: (verbose <= 0)});
  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log("\nERROR. Couldn't treat the tremolo result file.\n");
  } else {
    console.log("DONE!\n");
  }

  // removing tremolo output temporary files
  shell.rm("src/ISDB_tremolo_NP3/Data/results/*.out");
  shell.rm("src/ISDB_tremolo_NP3/Data/results/*.pklbin");
  shell.rm("src/ISDB_tremolo_NP3/Data/results/*.params");

  console.log("Tremolo search ended. "+printTimeElapsed(process.hrtime(start))+"\n");
  return(resExec.code);
}

function callCreateBatchLists(metadata, raw_data_path, output_path, output_name, processed_data_name, verbose)
{
  var start = process.hrtime();
  console.log('*** Calling R script to create the MSCluster specfication lists for '+output_name+' ***\n');
  try {
    var resExec = shell.exec('Rscript src/create_batch_lists.R ' + metadata + ' ' + raw_data_path + ' ' + output_path +
      ' ' + output_name + ' ' + processed_data_name, {async: false, silent: (verbose <= 0)});
  } catch (e) {
    //console.log(e);
    console.log("\nERROR");
  }

  if (resExec.code) {
    process.exit(1);
  }

  console.log('DONE! '+printTimeElapsed(process.hrtime(start))+"\n");
}

function renameTremoloJoinedIds(path_clean_count, path_tremolo_result, verbose)
{
  var start = process.hrtime();
  console.log('*** Calling R script to rename the tremolo result with the joined IDs ***\n');
  try {
    var resExec = shell.exec('Rscript src/tremolo_update_joinedIds.R ' + path_tremolo_result + ' ' + path_clean_count,
        {async: false, silent: (verbose <= 0)});
  } catch (e) {
    if (verbose <= 0) {
      console.log(e);
    }
    console.log("\nERROR");
  }

  console.log('DONE! '+printTimeElapsed(process.hrtime(start))+"\n");
}

function mergeTremoloResults(path_tremolo_result, max_results, path_count_files, verbose)
{
  var start = process.hrtime();
  console.log('*** Calling Python script merge the tremolo result with the count files ***\n');
  try {
    shell.exec('python3 src/ISDB_tremolo_NP3/Data/dbs/tremolo_merge_count.py ' + path_tremolo_result +
        ' ' + max_results + ' ' + path_count_files.join(' '), {async: false, silent: (verbose <= 0)});
  } catch (e) {
    if (verbose <= 0) {
      console.log(e);
    }
    console.log("\nERROR");
  }

  console.log('\nDONE! '+printTimeElapsed(process.hrtime(start))+"\n");
}

function callMetfragPubChem(output_name, output_path, method, ion_mode, ppm_tolerance, fragment_tolerance,scale_factor,
                            verbose)
{
  console.log('*** Calling R script to run MetFrag identification with PubChem for ' + output_name + ' ***\n');

  resExec = shell.exec('Rscript src/metfrag_autosearch.R ' + output_path + "/" + ' ' +
      method + ' ' +ion_mode + ' ' + ppm_tolerance + ' ' + fragment_tolerance +
      ' ' + scale_factor, {async: false, silent: (verbose <= 0)});
  // { code:..., stdout:... , stderr:... }
  if (resExec.code) {
    if (verbose <= 0) {
      console.log(resExec.stdout);
      console.log(resExec.stderr);
    }
    console.log('ERROR\n');
  } else {
    console.log('DONE!\n');
  }
}

function checkCountsConsistency(output_path, processed_data, metadata, clustering = false, clean = false, merge = false)
{
  output_name = basename(output_path, osSep());
  output_path = output_path + osSep()+ 'count_tables' + osSep();
  var res_all = "*** checkCountsConsistency of job "+output_name+" ***\n\n";
  // check the clustering counts
  if (clustering)
  {
    console.log('\n*** Testing the consistency of the clustering counts for the job '+output_name+' ***\n');
    resExec = shell.exec('Rscript test/test_counts.R ' + processed_data + ' '+ output_path+output_name+'_spectra.csv ' +
        output_path+output_name+'_peak_area.csv '+metadata, {async: false, silent: false});

    res_all = res_all + "*clustering*\n"+resExec.stdout+"\n"+resExec.stderr+"\n"
  }
  if (shell.test("-e", output_path+'clean'+osSep()) && clean)
  {
    console.log('\n*** Testing the consistency of the clean counts for the job '+output_name+' ***\n');
    resExec  = shell.exec('Rscript test/test_counts.R ' + processed_data + ' '+ output_path+'clean'+osSep()+output_name+
        '_spectra_clean_annotated.csv '+output_path+'clean'+osSep()+output_name+'_peak_area_clean_annotated.csv '+
        metadata, {async: false, silent: false});

    res_all = res_all + "*clean*\n"+resExec.stdout.toString()+"\n"+resExec.stderr.toString()+"\n"
  }
  if (shell.test("-e", output_path+'merge'+osSep()) && merge)
  {
    console.log('\n*** Testing the consistency of the merged counts for the job '+output_name+' ***\n');
    resExec = shell.exec('Rscript test/test_counts.R ' + processed_data+' '+ output_path+'merge'+osSep()+output_name+
        '_spectra_merged_iadmf.csv '+output_path+'merge'+osSep()+output_name+'_peak_area_merged_iadmf.csv '+
        metadata, {async: false, silent: false});

    res_all = res_all + "*merge*\n"+resExec.stdout+"\n"+resExec.stderr+"\n"
  }

  return res_all;
}

function checkCleanMNConsistency(output_path, sim_tol, mn_tol, rt_tol, mz_tol, top_k)
{
  output_name = basename(output_path, osSep());
  var res_all = "*** checkCleanMNConsistency of job "+output_name+" ***\n\n";

  // check molecular networking consistency
  console.log('\n*** Testing the consistency of the clean and annotated counts and the molecular networking for the job '+output_name+' ***\n');
  resExec = shell.exec('Rscript test/test_clean_mn.R ' + output_path+' '+sim_tol+' '+ mn_tol+' '+ rt_tol+' '+ mz_tol+' '+
      top_k, {async: false, silent: false});
  res_all = res_all + resExec.stdout.toString()+
      "\n"+resExec.stderr.toString() + "\n";

  return res_all;
}

function checkMNConsistency(output_path, mn_tol, top_k)
{
  output_name = basename(output_path, osSep());
  var res_all = "*** checkMNConsistency of job "+output_name+" ***\n\n";

  // check molecular networking consistency
  console.log('\n*** Testing the consistency of the molecular networking for the job '+output_name+' ***\n');
  resExec = shell.exec('Rscript test/test_mn.R ' + output_path+' '+mn_tol+' '+top_k, {async: false, silent: false});

  res_all = res_all + resExec.stdout.toString()+"\n"+resExec.stderr.toString() + "\n";
  return res_all;
}

function isWindows() {
  return process.platform === "win32" || process.platform === "win64";
}

function osSep()
{
  if (isWindows())
    return  "\\";
  else
    return "/";
}

function defaultModelDir() {
	if (isWindows())
		return "NP3_MSCluster\\Models_Windows";
	else
		return "./NP3_MSCluster/Models";
}

program
  .version('5.0.0',  '--version')
  .usage(' command [options]\n\n' +
      'A collection of scripts to enhance untargeted metabolomics research focused on drug discovery ' +
      'with optimizations towards natural products.\n\nThe NP3 Mass Spectrometry (MS) workflow proposes an automatized ' +
      'procedure with the capability to cluster (join) and quantify fragmented ions (MS/MS or MS2) associated with ' +
      'the same metabolite, which eluted in concurrent chromatographic peaks (MS1), of a collection of samples ' +
      'LC-MS/MS and to correlate this information with the samples bioactivity score. It generates a rank of mass ' +
      'candidates responsible for the obeserved hit in the bioactivity experiment.\n\n' +
      'This workflow can execute the following commands:');

program
  .command('setup')
  .description('Check if the NP3 dependencies are installed, try to install missing R and python packages and ' +
      'compile the NP3_MSCluster code\n')
  .action(function()
  {
    var start = process.hrtime();
    console.log('\nNP3 workflow setup\n\n');
    var resExec;
    var countError = 0;

    if (!shell.which('R')) {
      console.log('R not found, please ensure R is available and try again.');
    } else {
      // install R packages
      console.log('Checking R packages: stats, dplyr, purrr, inline, rJava, devtools, metfRag, XCMS and MSnBase\n');

      resExec = shell.exec('Rscript src/R_requirements.R', {async:false});
      if (resExec.code) {
        //console.log(resExec.stdout);
        //console.log(resExec.stderr);
        console.log('\nERROR. Could not install all R packages, retry with user privileges or do it manually.');

        countError = countError + 1;
      } else {
        console.log("DONE!\n");
      }
    }

	if (isWindows())
	{
		if (!shell.which('python3')) {
		  console.log('Python 3 not found, please ensure python 3 is available and try again.');
		} else {
			if (parseDecimal(shell.exec("python3 --version", {async:false, silent: true}).stdout.toString().split(' ')[1]) < 3)
				{
					console.log('Python 3 not found, please ensure python 3 is available and try again.');
				}
		}
	} else {
        if (!shell.which('python3')) {
		  console.log('Python 3 not found, please ensure python 3 is available and try again.');
		}
	}


    if (!shell.which('pip3')) {
      console.log('Pip3 not found, please ensure pip3 - the python 3 package management - is available and try again.');
    } else {
      // install python packages
      console.log('\nChecking python 3 packages: panda, numpy and scipy\n\n');
      resExec = shell.exec('pip3 install -r src/python_requirements --user', {async:false});
      if (resExec.code) {
        //console.log(resExec.stdout);
        //console.log(resExec.stderr);
        console.log('\nERROR. Could not install all python packages, retry with user privileges or do it manually.');
        countError = countError + 1;
      } else {
		  console.log("DONE!\n");
	  }
    }

    if (!shell.which('make')) {
      shell.echo('Make not found, please ensure make is available and try again.');
      shell.exit(1);
    }

    console.log('\nCompiling NP3-MS-Clustering\n');
    shell.cd('NP3_MSCluster');

    resExec = shell.exec('make clean', {async:false});
    if (resExec.code) {
      // console.log(resExec.stdout);
      // console.log(resExec.stderr);
      console.log('\nERROR. Could not clean the NP3_MSClustering compilation output. Ensure Make is installed and try again.');
      process.exit(1);
    }

    resExec = shell.exec('make', {async:false});
    if (resExec.code) {
      // console.log(resExec.stdout);
      // console.log(resExec.stderr);
      console.log('\nERROR. Could not compile the NP3_MSClustering algorithm. Ensure Make is working and try again.');
      process.exit(1);
    } else
    {
      console.log("DONE!\n\n");
    }

    shell.cd('..');

    if (countError === 0)
    {
      console.log('NP3 workflow installation complete! ' + printTimeElapsed(process.hrtime(start)))
    } else {
      console.log('NP3 workflow installation ended with ' + countError + ". " + printTimeElapsed(process.hrtime(start)))
    }

  })
  .on('--help', function() {
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow setup');
  });

program
  .command('run')
  .description('Steps 2 to 10: Run the entire NP3 workflow. (2) Pre process the raw MS/MS data enriching the ' +
      'spectra with chromatographic dimensions, if not done yet; (3) cluster the MS/MS pre processed data; (4) parse ' +
      'the results to compute the counts of spectra and of peak area of each resulting cluster ID by sample; (5) ' +
      'identify the clusters with the ISDB from the UNPD using tremolo; (6) compute the pairwise similarity comparisions ' +
      'of the resulting clusters consensus; (7) clean the counts and annotate spectra; (8) merge the counts based in the ' +
      'annotations; (9) compute the specified bioactivity correlations; (10) and create the similarity and annotation ' +
      'molecular networkings to visualize the results.\n\n')
  .option('-n, --output_name <name>',
      'job name. It will be used to name the output directory and\n\t\t\t\t\t' +
      'the results from the final integration step\n')
  .option('-m, --metadata <path>', 'path to the metadata table CSV file\n')
  .option('-d, --raw_data_path <path>', 'path to the folder containing the input MS/MS spectra raw\n\t\t\t\t\t' +
      'data in mzXML format\n')
  .option('-o, --output_path <path>', 'path to where the output directory will be created\n')
  .option('-f, --fragment_tolerance [x]', 'the tolerance in Daltons for fragment peaks. Peaks in the\n\t\t\t\t\t' +
      'original spectra that are closer than this get merged in\n\t\t\t\t\t' +
      'the clustering jobs (Step 3). Also used in the similarity\n\t\t\t\t\t' +
      'pairwise comparision (Step 6), in the clean and\n\t\t\t\t\t' +
      'annotation script (Step 7)',
      parseFloat, 0.05)
  .option('-z, --mz_tolerance [x]', 'this is the tolerance in Daltons for the m/z of the\n\t\t\t\t\t' +
      'precursor that determines if two spectra will be compared\n\t\t\t\t\t' +
      'and possibly joined. Used in the clustering jobs (Step 3),\n\t\t\t\t\t' +
      'in the cleaning and annotation script (Step 7) and in\n\t\t\t\t\t' +
      'identifications (Step 5)', parseFloat, 0.025)
  .option('-p, --ppm_tolerance [x]', 'the maximal tolerated m/z deviation in parts per million\n\t\t\t\t\t' +
      '(ppm) to be used in the pre processing (Step 2) and in the\n\t\t\t\t\t' +
      'MetFrag identification with PubChem. Typically set to a\n\t\t\t\t\t' +
      'generous multiple of the mass accuracy of the mass\n\t\t\t\t\t' +
      'spectrometer', parseFloat, 15)
  .option('-a, --ion_mode [x]', 'the precursor ion mode. One of the following numeric values\n\t\t\t\t\t' +
      'corresponding to an ion adduct type: \'1\' = [M+H]+ or\n\t\t\t\t\t' +
      '\'2\' = [M-H]-', convertIonMode,1)
  .option('-s, --similarity [x]', 'the minimum similarity to be consider in the hierarchical\n\t\t\t\t\t' +
      'clustering, starts in 0.70 and decrease to X in 15 rounds.\n\t\t\t\t\t' +
      'Also used in Steps 7 and 10', parseFloat, 0.55)
  .option('-g, --similarity_blank [x]', 'the minimum similarity to be consider in the hierarchical\n\t\t\t\t\t' +
      'clustering of the blank clustering steps, starts in 0.70\n\t\t\t\t\t' +
      'and decrease to X in 15 rounds. Only used in the \n\t\t\t\t\t' +
      'clustering of blank samples (column SAMPLE\\_TYPE equals\n\t\t\t\t\t' +
      '\'blank\' in the metadata table)', parseFloat, 0.3)
  .option('-w, --similarity_mn [x]', 'the minimum similarity score that must occur between a pair\n\t\t\t\t\t' +
      'of consensus spectra to connect them with an edge in the\n\t\t\t\t\t' +
      'molecular networking. Lower values will increase the\n\t\t\t\t\t' +
      'component size of the clusters by inducing the connection\n\t\t\t\t\t' +
      'of less related spectra; and higher values will limit\n\t\t\t\t\t' +
      'the components sizes to the opposite', parseFloat, 0.6)
  .option('-t, --rt_tolerance [x,y]', 'tolerances in seconds for the retention time width of the\n\t\t\t\t\t' +
      'precursor that determines if two spectra will be compared\n\t\t\t\t\t' +
      'and possibly joined. The first tolerance [x] is used in\n\t\t\t\t\t' +
      'the data, blank and batches integration clustering steps\n\t\t\t\t\t' +
      '(Step 3); and the tolerance [y] is used in the final\n\t\t\t\t\t' +
      'integration step (Step 3) and in Steps 2 and 7\n\t\t\t\t       ', splitListFloat, [1,2])
  .option('-k, --net_top_k [x]', 'Maximum number of connection for one single node in the\n\t\t\t\t\t' +
      'similarity molecular networking. An edge between two nodes\n\t\t\t\t\t' +
      'is kept only if both nodes are within each other\'s [x]\n\t\t\t\t\t' +
      'most similar nodes. This restriction is applied in the\n\t\t\t\t\t' +
      'msclusterID\'s order, so smaller m/z\'s are limited first.\n\t\t\t\t\t' +
      'Keeping this value low makes very large networks (many\n\t\t\t\t\t' +
      'nodes) much easier to visualize',10)
  .option('-c, --scale_factor [x]', 'the scaling method to be used in the fragmented peak\'s\n\t\t\t\t\t' +
      'intensities before any dot product comparision (Steps 3, 6\n\t\t\t\t\t' +
      'and 7). Valid values are: 0 for the natural logarithm (ln)\n\t\t\t\t\t' +
      'of the intensities; 1 for no scaling; and other values\n\t\t\t\t\t' +
      'greater than zero for raising the fragment peaks\n\t\t\t\t\t' +
      'intensities to the power of x (e.g. x = 0.5 is the square\n\t\t\t\t\t' +
      'root scaling). [x] >= 0', parseFloat, 0.5)
  .option('-l, --parallel_cores [x]', 'the number of cores to be used for parallel processing\n\t\t\t\t\t' +
      '(Step 6). x = 1 for disabling parallelization and x > 2\n\t\t\t\t\t' +
      'for enabling it. x >= 1', parseDecimal, 2)
  .option('-y, --processed_data_name [x]', 'the name of the folder inside the raw_data_path where the\n\t\t\t\t\t' +
      'pre process data will be stored. If the given folder do\n\t\t\t\t\t' +
      'not exists it will create it and pre process the raw data\n\t\t\t\t\t' +
      'using the default values of the missing arguments.\n\t\t\t\t\t' +
      'Otherwise it will depend on the processed_data_overwrite\n\t\t\t\t\t' +
      'parameter value', "processed_data")
  .option('-q, --processed_data_overwrite [x]', 'A logical "TRUE" or "FALSE" indicating if the pre processed\n\t\t\t\t\t' +
      'data present in the processed_data_name folder (if it\n\t\t\t\t\t' +
      'already exists) should be overwritten and pre processed\n\t\t\t\t\t' +
      'again', "FALSE")
  .option('-u, --rules [x]', ' path to the CSV file with the accepted ion modifications\n\t\t\t\t\t' +
      'rules. To be used in the spectra annotation step to detect\n\t\t\t\t\t' +
      'variants (Step 7)',"rules/np3_modifications.csv")
  .option('-r, --trim_mz [x]', 'A logical "TRUE" or "FALSE" indicating if the spectra should\n\t\t\t\t\t' +
      'be trimmed by the precursor mass +5 Da before the pairwise\n\t\t\t\t\t' +
      'comparisions. If "TRUE" it can potentially remove noise\n\t\t\t\t\t' +
      'peaks and improve the similarity scores',"TRUE")
  .option('-x, --min_peaks_output [x]', 'the minimum number of fragment peaks that a spectrum must\n\t\t\t\t\t' +
      'have to be outputted after the final clustering step (Step\n\t\t\t\t\t' +
      '3). Spectra with less than [x] peaks will be discarted.\n\t\t\t\t\t' +
      'x >= 1',5)
  .option('-b, --max_chunk_spectra [x]', "Maximum number of spectra (rows) to be loaded and processed\n\t\t\t\t\t" +
      "in a chunk at the same time. In case of memory issues this\n\t\t\t\t\t" +
      "value should be decreased",parseDecimal,3000)
  .option('-e, --method [name]', 'a character string indicating which correlation coefficient\n\t\t\t\t\t' +
      'is to be computed. One of "pearson", "kendall", or\n\t\t\t\t\t' +
      '"spearman"', convertMethodCorr,"spearman")
  .option('-i, --metfrag_identification [x]', 'a logical "TRUE" or "FALSE" indicating if the MetFrag tool\n\t\t\t\t\t' +
      'should be used for identification search against the\n\t\t\t\t\t' +
      'PubChem database of the top correlated m/z\'s\n\t\t\t\t\t', "FALSE")
  .option('-j, --tremolo_identification [x]', '(not Windows OS\'s) A logical "TRUE" or "FALSE" indicating if\n\t\t\t\t\t' +
      'the Tremolo tool should be used for the spectral matching\n\t\t\t\t\t' +
      'against the ISDB from the UNPD', "FALSE")
  .option('-v, --verbose [x]', 'for values X>0 show the scripts output information\n\t\t\t\t\t', parseDecimal, 0)
  .action(function(options) {
    // console.log(options);
    // check mandatory params
    if (typeof options.output_name === 'undefined') {
      console.error('\nMissing the mandatory \'output_name\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.metadata === 'undefined') {
      console.error('\nMissing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.raw_data_path === 'undefined') {
      console.error('\nMissing the mandatory \'raw_data_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.output_path === 'undefined' || options.output_path === "undefined") {
      console.error('\nMissing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    // check some parameters values
    ["similarity", "similarity_blank", "similarity_mn"].forEach(function(parm)
    {
      var value = options[parm];
      if (isNaN(value) || value > 1 || value < 0)
      {
        console.error('\nERROR. Wrong '+parm+' parameter value. The '+parm+' threshold must be a positive numeric value less than 1.0. Execution aborted.');
        process.exit(1);
      }
    });
    if (isNaN(options.fragment_tolerance) || options.fragment_tolerance < 0)
    {
      console.error('\nERROR. Wrong fragment_tolerance parameter value. The fragment tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.mz_tolerance) || options.mz_tolerance < 0)
    {
      console.error('\nERROR. Wrong mz_tolerance parameter value. The m/z mz_tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.ppm_tolerance) || options.ppm_tolerance < 0)
    {
      console.error('\nERROR. Wrong ppm_tolerance parameter value. The PPM tolerance must be a positive numeric value. Execution aborted.');
      process.exit(1);
    }

    var start = process.hrtime();
    // run workflow
    console.log('*** NP3 Workflow for ' +options.output_name+ ' ***\n');

    // set model dir
    // directory where model files are kept. If running MSCluster on Windows and not from the current
    // directory you should specify the path to 'Models_Windows'
    options.model_dir = defaultModelDir();
    // set num of rounds
    // determines how many rounds are used for the hierarchical clustering. [n] <= 20.
    options.num_rounds = 15;
    // set mixture probability
    // the probability wrongfully adding a spectrum to a cluster. [x] < 0.5
    options.mixture_prob = 0.4;

    var output_path = options.output_path+osSep()+options.output_name;
    var specs_path = output_path + osSep()+ "spec_lists";

    // check if the raw data is processed, if not process it with the default parms
    // always call pre process script, let it decide if it needs to perfom some action (missing processed files) or not
    callProcessData(options.output_name, options.metadata, options.raw_data_path, options, options.verbose);

    if (shell.test('-e', output_path))
    {
      console.log("The provided output path already exists, delete it or change the output directory name and retry. " + output_path);
      process.exit();
      //shell.rm('-r', output_path)
    }

    callCreateBatchLists(options.metadata, options.raw_data_path, options.output_path, options.output_name,
      options.processed_data_name, options.verbose);

    // save run parameters values
    shell.ShellString('output_name: '+options.output_name + "\n\ncmd: \n\n").toEnd(output_path+'/logRunParms');
    shell.ShellString(options.parent.rawArgs.join(' ')).toEnd(output_path+'/logRunParms');

    // copy rules
    shell.cp(options.rules, output_path);

    callClustering(options, output_path, specs_path);

    // clean output folder
    // remove specs folder
    shell.rm('-rf', specs_path);

    // tremolo search for not windows OS
    if (!isWindows() && options.tremolo_identification === "TRUE")
    {
      tremoloIdentification(options.output_name, output_path + "/outs/" + options.output_name + "/identifications",
          output_path+"/outs/"+options.output_name+"/mgf/"+options.output_name+"_all.mgf",
          options.mz_tolerance,0.2, 10, options.verbose, 0)
    }

    output_path = output_path+"/outs/"+options.output_name;

    // cread molecular networking output dir
    shell.mkdir("-p", output_path+ "/molecular_networking/similarity_tables");
    // call pairwise comparision, create folder
    callPairwiseComparision(options.output_name, output_path + "/molecular_networking/similarity_tables",
        output_path+"/mgf/", options.fragment_tolerance,
        options.scale_factor, options.trim_mz, options.parallel_cores, options.verbose);

    // remove mass dissipation and annotate clustering from area and spectra count
    resExec = callCleanAnnotateClustering(options, output_path, options.mz_tolerance,
      options.rt_tolerance[1], options.fragment_tolerance);

    var counts_path = output_path+"/count_tables/"+options.output_name;
    //resExec = 0
    if (!resExec) // if the clean was succesful, continue for correlation and merge
    {
      counts_path = output_path+"/count_tables/clean/"+options.output_name;
      // for analysing the clean clustering count
      callAnalyseCount(counts_path+"_spectra_clean_annotated.csv",
          output_path+"/count_tables/clean/analyseCountClusteringClean");

      // rename msclusterID from tremolo result with the joined ids and merge results with counts files
      if (!isWindows() && options.tremolo_identification === "TRUE") {
        renameTremoloJoinedIds(counts_path+"_spectra_clean_annotated.csv",
            output_path+ "/identifications/tremolo_results.csv", options.verbose);
        mergeTremoloResults(output_path + "/identifications/tremolo_results.csv",
            10, [counts_path+"_spectra_clean_annotated.csv",
              counts_path+"_peak_area_clean_annotated.csv"]);
      }

      // call correlation for the cleaned peak area count and spectra area count
      callComputeCorrelation(options.metadata, counts_path+"_peak_area_clean_annotated.csv",
        options.method, 0, options.verbose);

      callComputeCorrelation(options.metadata, counts_path+"_spectra_clean_annotated.csv",
          options.method, 0, options.verbose);

      callMergeCounts(output_path, options.output_name,
          options.raw_data_path + osSep() + options.processed_data_name, options.metadata,
          options.method, options.verbose);
    } else {
      // the clean was not succesfull  corr not clean counts

      // merge tremolo results with counts files
      if (!isWindows() && options.tremolo_identification === "TRUE") {
        mergeTremoloResults(output_path + "/identifications/tremolo_results.csv",
            10, [counts_path+"_spectra.csv",
              counts_path+"_peak_area.csv"]);
      }

      // call correlation for the mscluster peak area count
      callComputeCorrelation(options.metadata, counts_path+"_peak_area.csv",
          options.method, 0, options.verbose);

      // call correlation for the mscluster spectra count
      callComputeCorrelation(options.metadata, counts_path+"_spectra.csv",
          options.method, 0,  options.verbose);
    }

    callCreatMN(output_path+"/", options.similarity_mn, options.net_top_k,
        options.max_chunk_spectra,options.verbose);

    if (options.metfrag_identification === "TRUE")
    {
      callMetfragPubChem(options.output_name, output_path, options.method,
          options.ion_mode, 3, options.fragment_tolerance,
          options.scale_factor, options.verbose);
    }

    console.log('*** Done for '+options.output_name+' ***');
    console.log(printTimeElapsed(process.hrtime(start)));

    if (options.verbose >= 10) {
      if (test_res[options.output_name] === undefined) {
        test_res[options.output_name] = "";
      }
      test_res[options.output_name] = test_res[options.output_name] + checkCleanMNConsistency(output_path,options.similarity, options.similarity_mn,
          options.rt_tolerance[1], options.mz_tolerance, options.net_top_k);

      test_res[options.output_name] = test_res[options.output_name] + checkCountsConsistency(output_path,options.raw_data_path+osSep()+options.processed_data_name,
          options.metadata, true, true, true);
    }
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Results:');
    console.log('');
    console.log("A directory inside the *output\_path* named with the *output\_name* containing:\n" +
        "- A copy of the *metadata* and the *rules* files and the command line parameters values used in a file " +
        "named 'logRunParms', for reproducibility\n" +
        "- a folder named 'outs' with the clustering steps results in separate folders containing:\n" +
        "- A subfolder named 'count_tables' with the Step 4 quantifications in CSV tables named as " +
        "'<step\_name>\_(spectra|peak\_area).csv'.\n" +
        "- Onother subfolder named 'clust' with the clusters membership files (which SCANS or msclusterID were joined)\n" +
        "- A third subfolder named 'mgf' with the resulting clusters consensus spectra in MGF files\n" +
        "- A text file named 'logRun' with the NP3\_MSCluster log output.\n\n" +
        "The clustering steps results folders are named as 'B\_\<DATA_COLLECTION_BATCH\>\_\<X\>' where " +
        "*\<DATA\_COLLECTION\_BATCH\>* is the data collection batch number in the metadata file of each group of " +
        "samples and *\<X\>* is 0 if it is a data clustering step or 1 if it is a blank clustering step.\n\n" +
        "The final integration step result is located inside the 'outs' directory in a folder named with the " +
        "*output\_name* and it also contains the following results:\n" +
        "- CSV tables with the counts from Steps 7 and 8 inside the 'count_tables' folder in subfolders named " +
        "'clean' and 'merge';\n" +
        "- the tremolo and the MetFrag identification results inside the 'identifications' folder\n" +
        "- the pairwise tables in subfolders of the 'molecular_networking' folder named 'annotation_table' and 'similarity_tables'\n" +
        "- the molecular networking's edge files in the 'molecular_networking' folder, the annotation network is " +
        "named as '<*output\_name*>_molecular_networking_annotation.selfloops' and the similarity network is " +
        "named as '<*output\_name*>_molecular_networking_sim<*similarity_mn*>_k<*net_top_k*>.selfloops'");
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow run --output_name "test_np3" --output_path "/path/where/the/output/will/be/stored" ' +
      '--metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --raw_data_path "/path/to/the/raw/data/dir"');
    console.log('');
    console.log('  $ node np3_workflow run -n "test_np3_rt_tol" -o "/path/where/the/output/will/be/stored" ' +
      '-m "/path/to/the/metadata/file/test_np3_metadata.csv" -d "/path/to/the/raw/data/dir" ' +
      '-t 3.5,5 -v 1');
  });

program
  .command('pre_process')
  .description('Step 2: Run the script that pre processes the LC-MS/MS raw data in mzXML format. It extracts ' +
      'the MS1 peaks dimension informations (retention time minimum and maximum, the peak area and the peaks ID) of ' +
      'each sample, matches its MS2 ions retention time and precursor m/z against these list of MS1 peaks and assign ' +
      'to each spectra a peak dimension that includes it. Additionaly a table with the peaks without a spectrum m/z and ' +
      'retention time match are stored in a count table to represent the not fragmented m/z\'s.\n\n')
  .option('-n, --data_name <name>', 'the data collection name for verbosity\n')
  .option('-m, --metadata <path>', 'path to the metadata table CSV file\n')
  .option('-d, --raw_data_path <path>', 'path to the folder containing the input MS/MS spectra raw\n\t\t\t\t\t' +
      'data in mzXML format\n')
  .option('-y, --processed_data_name [x]', 'The name of the output directory that should be created\n\t\t\t\t\t' +
      'inside the raw_data_path to store the processed data. When\n\t\t\t\t\t' +
      'not using the default directory, this value should be\n\t\t\t\t\t' +
      'informed in the *run* or *clustering* commands\n\t\t\t\t\t', "processed_data")
  .option('-t, --rt_tolerance [x]', 'the tolerance in seconds used to enlarge the peak boundaries\n\t\t\t\t\t' +
      'and accept as a match all MS2 ions that have a very close\n\t\t\t\t\t' +
      'by retention time. Tries to overcome bad MS1 peak\n\t\t\t\t\t' +
      'integrations.', parseFloat, 2)
  .option('-z, --mz_tolerance [x]', 'the tolerance in Daltons for two precursor m/z\'s be\n\t\t\t\t\t' +
      'considered the same.', parseFloat, 0.05)
  .option('-p, --ppm_tolerance [x]', 'the maximal tolerated m/z deviation in consecutive scans in\n\t\t\t\t\t' +
      'parts per million (ppm) for the initial ROI definition of\n\t\t\t\t\t' +
      'the R::xcms::centWave algorithm. Typically set to a\n\t\t\t\t\t' +
      'generous multiple of the mass accuracy of the mass\n\t\t\t\t\t' +
      'spectrometer.', parseFloat, 15)
    .option('-a, --ion_mode [x]', 'the precursor ion mode. One of the following numeric values\n\t\t\t\t\t' +
        'corresponding to a ion adduct type: \'1\' = [M+H]+ or\n\t\t\t\t\t' +
        '\'2\' = [M-H]-', convertIonMode,1)
  .option('-e, --peak_width [X,Y]', 'two numerics separated by comma without spaces and using\n\t\t\t\t\t' +
      'decimal point equals dot, containing the expected\n\t\t\t\t\t' +
      'approximate peak width in chromatographic space. Given\n\t\t\t\t\t' +
      'as a range (min, max) in seconds. The max value will be\n\t\t\t\t\t' +
      'used to simulate the width of the fake peaks (see\n\t\t\t\t\t' +
      'documentation)', "2,5")
  .option('-s, --snthresh [x]', 'a numeric defining the signal to noise ratio cutoff.\n\t\t\t\t\t',
      parseFloat, 0)
  .option('-f, --pre_filter [k,I]', 'two numerics separated by comma "," without spaces and with\n\t\t\t\t\t' +
      'decimal point equals dot "." specifying the prefilter step\n\t\t\t\t\t' +
      'for the first analysis step (ROI detection) of the R::xcms\n\t\t\t\t\t' +
      'centWave algorithm. Mass traces are only retained if they\n\t\t\t\t\t' +
      'contain at least k peaks with intensity >= I.\n\t\t\t\t\t', "1,750")
  .option('-i, --min_fraction [x]', 'a numeric defining the minimum fraction of samples in at\n\t\t\t\t\t' +
      'least one sample group in which the peaks have to be\n\t\t\t\t\t' +
      'present to be considered as a peak group (feature). Used\n\t\t\t\t\t' +
      'in the alignment process to define hook peaks\n\t\t\t\t\t', parseFloat, 0.3)
  .option('-w, --bw [x]', 'a numeric defining the bandwidth (standard deviation of the\n\t\t\t\t\t' +
      'smoothing kernel) to be used. This argument is passed to\n\t\t\t\t\t' +
      'the density method.', parseFloat, 1)
  .option('-b, --bin_size [x]', 'a numeric defining the size of the overlapping slices in mz\n\t\t\t\t\t' +
      'dimension.', parseFloat, 0.1)
  .option('-x, --max_features [x]', 'a numeric with the maximum number of peak groups to be\n\t\t\t\t\t' +
      'identified in a single mz slice.', parseFloat, 100)
  .option('-r, --noise [x]', 'a numeric allowing to set a minimum intensity required for\n\t\t\t\t\t' +
      'centroids to be considered in the first analysis step\n\t\t\t\t\t' +
      '(centroids with intensity < noise are omitted from ROI\n\t\t\t\t\t' +
      'detection).', parseFloat, 500)
  .option('-c, --mz_center_fun [name]', 'Name of the function to calculate the m/z center of the\n\t\t\t\t\t' +
      'chromatographic peak. Allowed values are: "wMean":\n\t\t\t\t\t' +
      'intensity weighted mean of the peak\'s m/z values,\n\t\t\t\t\t' +
      '"mean": mean of the peak\'s m/z values, "apex": use\n\t\t\t\t\t' +
      'the m/z value at the peak apex, "wMeanApex3": intensity\n\t\t\t\t\t' +
      'weighted mean of the m/z value at the peak apex and the\n\t\t\t\t\t' +
      'm/z values left and right of it and "meanApex3": mean of\n\t\t\t\t\t' +
      'the m/z value of the peak apex and the m/z values left\n\t\t\t\t\t' +
      'and right of it.', "wMeanApex3")
  .option('-j, --max_samples_batch_align [x]', 'The maximum number of not blank samples to be uniformly\n\t\t\t\t\t' +
      'selected for the batch alignment. Due to memory issues\n\t\t\t\t\t' +
      'this value should not exceed 15 samples to avoid crashing\n\t\t\t\t\t' +
      'the script. If x == 0 diable alignment.', parseDecimal,5)
  .option('-u, --integrate_method [x]', 'Integration method for the XCMS CentWave algorithm. For\n\t\t\t\t\t' +
      'integrate = 1 peak limits are found through descent on the\n\t\t\t\t\t' +
      'mexican hat filtered data, for integrate = 2 the descent\n\t\t\t\t\t' +
      'is done on the real data. The latter method is more\n\t\t\t\t\t' +
      'accurate but prone to noise, while the former is more\n\t\t\t\t\t' +
      'robust, but less exact.', parseDecimal,2)
  .option('-g, --fit_gauss [x]', "a logical \"TRUE\" or \"FALSE\" indicating whether or not a\n\t\t\t\t\t" +
      "Gaussian should be fitted to each peak.","FALSE")
  .option('-q, --processed_data_overwrite [x]', 'A logical "TRUE" or "FALSE" indicating if the pre processed\n\t\t\t\t\t' +
        'data present in the processed_data_name folder should be\n\t\t\t\t\t' +
      'overwritten and pre processed again if it already exists\n\t\t\t\t\t', "TRUE")
  .option('-v, --verbose [x]', 'for values x>0 show the script output information\n\t\t\t\t\t', parseDecimal,0)
  .action(function(options) {
    // console.log(options)
    if (typeof options.data_name === 'undefined') {
      console.error('\nMissing the mandatory \'data_name\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.metadata === 'undefined') {
      console.error('\nMissing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.raw_data_path === 'undefined') {
      console.error('\nMissing the mandatory \'raw_data_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (isNaN(options.rt_tolerance) || options.rt_tolerance < 0)
    {
      console.error('\nERROR. Wrong rt_tolerance parameter value. The retention time tolerance must be a positive numeric value given in seconds. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.mz_tolerance) || options.mz_tolerance < 0)
    {
      console.error('\nERROR. Wrong mz_tolerance parameter value. The m/z tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.ppm_tolerance) || options.ppm_tolerance < 0)
    {
      console.error('\nERROR. Wrong ppm_tolerance parameter value. The PPM tolerance must be a positive numeric value. Execution aborted.');
      process.exit(1);
    }

    var start = process.hrtime();
    // run process peak info and align raw data
    console.log('*** NP3 Pre Process raw data to extract chromatogram peak info from MS1 to match with MS2 and to align samples ***\n');

    callProcessData(options.data_name, options.metadata, options.raw_data_path, options, options.verbose);

    console.log('*** Pre processed samples of '+options.data_name+' ***');
    console.log(printTimeElapsed(process.hrtime(start)));
  })
  .on('--help', function() {
    console.log('');
    console.log('If *max_samples_batch_align* > 0, align the samples to obtain a good guess for the retention time ' +
        'tolerances between samples of the same data collection batch and between all samples.');
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Result:');
    console.log('');
    console.log('\- A folder named *processed_data_name* inside the *raw_data_path* with one MGF per sample containing ' +
        'all the detected MS2 ions enriched with their respectively matched MS1 peak dimensions, e.g. retention time ' +
        'minimum, retention time maximum, peak area and peak ID. Each pre processed file is named with the SAMPLE_CODE ' +
        'defined in the metadata file followed by the string "_peak_info.mgf". ' +
        '\n\- If *max_samples_batch_align* > 0 a CSV file named "samples_alignment.csv" ' +
        'is created with the alignment value in seconds by data collection batch and of all samples together.\n\- ' +
        'A count file named "mzs_no_MS2.csv" with the quantification by sample of all the MS1 peaks that were not ' +
        'assigned to any MS2 ion and thus will be missing in the final clustering counts.\n\- A file named ' +
        '“log_no_peak_match.csv” is always created with all the MS2 m/z\'s ions that did not have a match with a MS1 ' +
        'peak and the reason it did not find a match: *no_mz* means no MS1 peak with that mz within the given tolerance ' +
        'was found and *no_rt* means no MS1 peak with that mz had a retention time range in which the MS2 ion retention ' +
        'time was contained within the given tolerance.\n\- A file named "parameters.csv" is always created with ' +
        'the parameters values used, for reproducibility.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('- Pre-processing with the default parameters values and align using at most 6 samples per data collection batch:');
    console.log(' $ node np3_workflow.js pre_process \-\-data_name "data_UHPLC_qTOF" \-\-metadata ' +
      '"/path/to/the/metadata/file/test_np3_metadata.csv" \-\-raw_data_path "/path/to/the/raw/data/dir" \-\-max_samples_batch_align 6 \-\-verbose 1');
    console.log('');
    console.log('- Pre-processing without alignment and with a different m/z tolerance:');
    console.log(' $ node np3_workflow.js pre_process \-\-data_name "data_UHPLC_qTOF" \-m ' +
      '"/path/to/the/metadata/file/test_np3_metadata.csv" \-d "/path/to/the/raw/data/dir" \-j 0 \-z 0.07');
  });

program
  .command('clustering')
  .description('Steps 3 and 4: Run the NP3_MSCluster algorithm to cluster the MS/MS data and two R scripts to ' +
      'quantify the number of spectra and of peaks area by sample for each final cluster ID. Library search using ' +
      'Tremolo and MetFrag R API' +
      '\n\n')
  .option('-n, --output_name <name>', 'job name. It will be used to name the output directory and the\n\t\t\t\t\t' +
      'final integration step result\n')
  .option('-m, --metadata <file>', 'path to the metadata table CSV file\n')
  .option('-d, --raw_data_path <path>', 'path to the folder containing the input MS/MS spectra raw data\n\t\t\t\t\t' +
      'in mzXML format\n')
  .option('-o, --output_path <path>', 'path to where the output directory will be created\n')
  .option('-f, --fragment_tolerance [x]', 'the tolerance in Daltons for fragment peaks. Peaks in the\n\t\t\t\t\t' +
      'original spectra that are closer than this get merged in\n\t\t\t\t\t' +
      'the NP3_MSCluster pre-processing', parseFloat, 0.05)
  .option('-z, --mz_tolerance [x]', 'this is the tolerance in Daltons for the m/z of the\n\t\t\t\t\t' +
      'precursor that determines if two spectra will be compared\n\t\t\t\t\t' +
      'and possibly joined. Used in the clustering job and\n\t\t\t\t\t' +
      'in the library identifications (Step 5)', parseFloat, 0.025)
  .option('-p, --ppm_tolerance [x]', 'the maximal tolerated m/z deviation in parts per million (ppm)\n\t\t\t\t\t' +
    'to be used in the MetFrag identification with PubChem\n\t\t\t\t\t', parseFloat, 5)
  .option('-a, --ion_mode [x]', 'the precursor ion mode. One of the following numeric values\n\t\t\t\t\t' +
      'corresponding to a ion adduct type: \'1\' = [M+H]+ or \n\t\t\t\t\t\'2\' = [M-H]-', convertIonMode,1)
  .option('-s, --similarity [x]', 'the minimum similarity to be consider in the hierarchical\n\t\t\t\t\t' +
      'clustering, starts in 0.70 and decrease to X in 15 rounds\n\t\t\t\t\t', parseFloat, 0.55)
  .option('-g, --similarity_blank [x]', 'the minimum similarity to be consider in the hierarchical\n\t\t\t\t\t' +
      'clustering of the blank clustering steps, starts in 0.70\n\t\t\t\t\t' +
      'and decrease to X in 15 rounds. Only used in the \n\t\t\t\t\t' +
      'clustering of blank samples (column SAMPLE\\_TYPE equals\n\t\t\t\t\t' +
      '\'blank\' in the metadata table)', parseFloat, 0.3)
  .option('-t, --rt_tolerance [x,y]', 'tolerances in seconds for the retention time width of the\n\t\t\t\t\t' +
      'precursor that determines if two spectra will be compared\n\t\t\t\t\t' +
      'and possibly joined. The first tolerance [x] is used in\n\t\t\t\t\t' +
      'the data, blank and batches integration clustering steps;\n\t\t\t\t\t' +
      'and the second tolerance [y] is for the final integration\n\t\t\t\t\t' +
      'step between all samples', splitListFloat, [1,2])
  .option('-y, --processed_data_name [x]', 'the name of the folder inside the raw_data_path where the\n\t\t\t\t\t' +
      'pre process data was stored', "processed_data")
  .option('-c, --scale_factor [x]', 'the scaling method to be used in the fragmented peak\'s\n\t\t\t\t\t' +
      'intensities before any dot product comparision (Step 3).\n\t\t\t\t\t' +
      'Valid values are: 0 for the natural logarithm (ln) of the\n\t\t\t\t\t' +
      'intensities; 1 for no scaling; and other values greater\n\t\t\t\t\t' +
      'than zero for raising the fragment peaks intensities to\n\t\t\t\t\t' +
      'the power of x (e.g. x = 0.5 is the square root scaling).\n\t\t\t\t\t' +
      '[x] >= 0', parseFloat, 0.5)
  .option('-e, --method [name]', 'a character string indicating which correlation coefficient is\n\t\t\t\t\t' +
      'to be computed. One of "pearson", "kendall", or "spearman"\n\t\t\t\t\t', convertMethodCorr,"spearman")
  .option('-x, --min_peaks_output [x]', 'the minimum number of fragment peaks that a spectrum must have\n\t\t\t\t\t' +
      'to be outputted after the final clustering step. Spectra\n\t\t\t\t\t' +
      'with less than x peaks will be discarted. x >= 1\n\t\t\t\t\t',5)
  .option('-i, --metfrag_identification [x]', 'a logical "TRUE" or "FALSE" indicating if MetFrag tool should\n\t\t\t\t\t' +
      'be used for identification search against the PubChem\n\t\t\t\t\t' +
      'database of the top correlated m/z\'s.', "FALSE")
  .option('-j, --tremolo_identification [x]', '(not Windows OS\'s) A logical "TRUE" or "FALSE" indicating if\n\t\t\t\t\t' +
      'the Tremolo tool should be used for the spectral matching\n\t\t\t\t\t' +
      'against the ISDB from the UNPD', "FALSE")
  .option('-b, --max_chunk_spectra [x]', "Maximum number of spectra to be loaded and processed in a\n\t\t\t\t\t" +
      "chunk at the same time. In case of memory issues this\n\t\t\t\t\t" +
      "value should be decreased",parseDecimal,3000)
  .option('-v, --verbose [x]', 'for values X>0 show the scripts output information\n\t\t\t\t\t', parseDecimal, 0)
  .action(function(options) {
    //console.log(options.output_path)
    // check mandatory params
    if (typeof options.output_name === 'undefined') {
      console.error('\nMissing the mandatory \'output_name\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.metadata === 'undefined') {
      console.error('\nMissing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.raw_data_path === 'undefined') {
      console.error('\nMissing the mandatory \'raw_data_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.output_path === 'undefined' || options.output_path === "undefined") {
      console.error('\nMissing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    // check some parameters values
    ["similarity", "similarity_blank"].forEach(function(parm)
    {
      var value = options[parm];
      if (isNaN(value) || value > 1 || value < 0)
      {
        console.error('\nERROR. Wrong '+parm+' parameter value. The '+parm+' threshold must be a positive numeric value less than 1.0. Execution aborted.');
        process.exit(1);
      }
    });
    if (isNaN(options.fragment_tolerance) || options.fragment_tolerance < 0)
    {
      console.error('\nERROR. Wrong fragment_tolerance parameter value. The fragment tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.mz_tolerance) || options.mz_tolerance < 0)
    {
      console.error('\nERROR. Wrong mz_tolerance parameter value. The m/z mz_tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.ppm_tolerance) || options.ppm_tolerance < 0)
    {
      console.error('\nERROR. Wrong ppm_tolerance parameter value. The PPM tolerance must be a positive numeric value. Execution aborted.');
      process.exit(1);
    }

    var start = process.hrtime();
    // run workflow
    console.log('*** NP3 Workflow for ' +options.output_name+ ' ***\n');

    // set model dir
    // directory where model files are kept. If running MSCluster on Windows and not from the current
    // directory you should specify the path to 'Models_Windows'
    options.model_dir = defaultModelDir();
    // set num of rounds
    // determines how many rounds are used for the hierarchical clustering. [n] <= 20.
    options.num_rounds = 15;
    // set mixture probability
    // the probability wrongfully adding a spectrum to a cluster. [x] < 0.5
    options.mixture_prob = 0.4;

    var output_path;
    var specs_path;

    if (isWindows())
    {
      output_path = options.output_path+"\\"+options.output_name;
      specs_path = output_path + "\\spec_lists";
    } else {
      output_path = options.output_path+"/"+options.output_name;
      specs_path = output_path + "/spec_lists";
    }

    // check if the raw data is processed, if not process it with the default parms
    // always call pre process script, let it decide if it needs to perfom some action (missing processed files) or not
    callProcessData(options.output_name, options.metadata, options.raw_data_path, options, options.verbose);


    if (shell.test('-e', output_path))
    {
      console.log("The provided output path already exists, delete it or change the output directory name and retry. " + output_path);
      process.exit()
      //shell.rm('-r', output_path)
    }

    callCreateBatchLists(options.metadata, options.raw_data_path, options.output_path, options.output_name,
      options.processed_data_name, options.verbose);

    // save run parameters values
    // save run parameters values
    shell.ShellString('output_name: '+options.output_name + "\n\ncmd: \n\n").toEnd(output_path+'/logRunParms');
    shell.ShellString(options.parent.rawArgs.join(' ')).toEnd(output_path+'/logRunParms');

    callClustering(options, output_path, specs_path);

    // the clean was not succesfull, search tremolo and corr and search not clean files
    // tremolo search for not windows OS
    if (!isWindows() && options.tremolo_identification === "TRUE")
    {
      tremoloIdentification(options.output_name, output_path + "/outs/" + options.output_name + "/identifications",
        output_path+"/outs/"+options.output_name+"/mgf/"+options.output_name+"_all.mgf",
        options.mz_tolerance,0.2, 10, options.verbose, 0)
      mergeTremoloResults(output_path + "/outs/" + options.output_name + "/identifications/tremolo_results.csv",
          10, [output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_spectra.csv",
            output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_peak_area.csv"])
    }

    // call correlation for the mscluster peak area count
    callComputeCorrelation(options.metadata, output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_peak_area.csv",
      options.method, 0, options.verbose);

    // call correlation for the mscluster spectra count
    callComputeCorrelation(options.metadata, output_path+"/outs/"+options.output_name+"/count_tables/"+options.output_name+"_spectra.csv",
      options.method, 0,  options.verbose);

    if (options.metfrag_identification === "TRUE")
    {
      callMetfragPubChem(options.output_name, output_path, options.method,
          options.ion_mode, 3, 0.003,
          options.scale_factor, options.verbose);
    }

    console.log('*** Done for '+options.output_name+' ***');
    console.log(printTimeElapsed(process.hrtime(start)));

    if (options.verbose >= 10) {
      if (test_res[options.output_name] === undefined) {
        test_res[options.output_name] = "";
      }
      test_res[options.output_name] = test_res[options.output_name] +  checkCountsConsistency(output_path+"/outs/"+options.output_name,options.raw_data_path+osSep()+options.processed_data_name,
          options.metadata, true, false, false);
    }
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Results:');
    console.log('');
    console.log("A directory inside the *output\_path* named with the *output\_name* containing:\n" +
        "- A copy of the *metadata* file and the command line parameters values used in a file " +
        "named 'logRunParms', for reproducibility\n" +
        "- a folder named 'outs' with the clustering steps results in separate folders containing:\n" +
        "- A subfolder named 'count_tables' with the Step 4 quantifications in CSV tables named as " +
        "'<step\_name>\_(spectra|peak\_area).csv'.\n" +
        "- Onother subfolder named 'clust' with the clusters membership files (which SCANS or msclusterID were joined)\n" +
        "- A third subfolder named 'mgf' with the resulting clusters consensus spectra in MGF files\n" +
        "- A text file named 'logRun' with the NP3\_MSCluster log output.\n\n" +
        "The clustering steps results folders are named as 'B\_\<DATA_COLLECTION_BATCH\>\_\<X\>' where " +
        "*\<DATA\_COLLECTION\_BATCH\>* is the data collection batch number in the metadata file of each group of " +
        "samples and *\<X\>* is 0 if it is a data clustering step or 1 if it is a blank clustering step.\n\n" +
        "The final integration step result is located inside the 'outs' directory in a folder named with the " +
        "*output\_name* and it also contains the tremolo and the MetFrag identification results inside the " +
        "'identifications' folder.");
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js clustering --output_name "test_np3" --output_path "/path/where/the/output/will/be/stored" ' +
      '--metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --raw_data_path "/path/to/the/raw/data/dir"');
    console.log('');
    console.log('  $ node np3_workflow.js clustering -n "test_np3_rt_tol" -o "/path/where/the/output/will/be/stored" ' +
      '-m "/path/to/the/metadata/file/test_np3_metadata.csv" -d "/path/to/the/raw/data/dir" ' +
      '-t 3.5,5');
  });

program
    .command('tremolo')
    .description('Step 5 : Run the tremolo tool used for the spectral matching against the In-Silico predicted MS/MS ' +
        'spectrum of Natural Products Database (ISDB) from the UNPD (Universal Natural Products Database)\n\n')
    .option('-o, --output_path <path>', 'path to where the spectral library search results will be stored')
    .option('-g, --mgf <path>', 'path to the input MGF file with the MS/MS spectra data to be searched and identified')
    .option('-c, --count_files [name]', 'optional paths to the count CSV files, separated by a comma and no space, ' +
        'as outputted by the NP3 workflow where the top_k search results by m/z should be added in 8 identification columns. ' +
        'Header must be in the first row.', splitList, [])
    .option('-z, --mz_tolerance [x]', 'the ± tolerance for parent mass search in Da. Set a small tolerance for ' +
        'dereplication using parent ion mass as prefilter, keeping in mind the resolution of your data. Increase to the ' +
        'wanted range for variable dereplication search (ex: 100 or 200 Da) Caution as this will also increase calculation times!',
        parseFloat, 0.025)
    .option('-s, --similarity [x]', 'the similarity threshold that determines if two spectra are the same. ' +
        'Should be kept low when using in-silico DB. Typically 0.2 to 0.3', parseFloat, 0.2)
    .option('-k, --top_k [x]', 'defines the maximal number of results returned', parseDecimal, 10)
    .option('-v, --verbose [x]', 'for values X>0 show the scripts output information', parseDecimal, 0)
    .action(function(options) {
      if (isWindows())
      {
        console.error('Tremolo search is only available for Unix OS.');
        process.exit(1);
      }

      if (typeof options.mgf === 'undefined') {
        console.error('Missing the mandatory \'mgf\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
        process.exit(1);
      }
      if (typeof options.output_path === 'undefined') {
        console.error('Missing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
        process.exit(1);
      }

      //call tremoloIdentification(output_name, output_path, count_files, mgf, mz_tol, sim_tol, top_k, verbose, verbose search)
      tremoloIdentification("tremolo_identification", options.output_path, options.mgf,
          options.mz_tolerance, options.similarity, options.top_k, options.verbose, options.verbose);
      if (!(options.count_files === undefined) && options.count_files.length > 0) {
        mergeTremoloResults(options.output_path + "/tremolo_results.csv", options.top_k, options.count_files);
      }
    })
    .on('--help', function() {
      console.log('');
      console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
      console.log('');
      console.log('Examples:');
      console.log('');
      console.log('  $ node np3_workflow.js tremolo --output_path "/path/to/the/output/dir/test_np3/outs/test_np3/identifications"' +
          ' --mgf "/path/to/the/output/dir/test_np3/outs/test_np3/mgf/test_np3_all.mgf"');
      console.log('');
      console.log('Concatenate tremolo results in the count files');
      console.log('');
      console.log('  $ node np3_workflow.js tremolo -o "/path/to/the/output/dir/test_np3/outs/test_np3/identifications"' +
          ' -g "/path/to/the/output/dir/test_np3/outs/test_np3/mgf/test_np3_all.mgf" ' +
          '-c "/path/to/the/output/dir/test_np3/outs/test_np3/test_np3_spectra_clean_annotated,' +
          '/path/to/the/output/dir/test_np3/outs/test_np3/test_np3_peak_area_clean_annotated"');
    });

program
  .command('clean_annotate')
  .description(' Step 6 and 7: Run the script to compute the pairwise comparision of the consensus clusters (if not ' +
      'present) and then run the script to clean the clustering counts and annotate possible ion variants. ' +
      'It also runs Step 10 to overwrite any old computation of the molecular networking\n\n')
  .option('-m, --metadata <file>', 'path to the metadata table CSV file')
  .option('-o, --output_path <path>', 'path to the output data folder, inside the outs directory of the clustering result folder. ' +
    'It should contain the mgf folder, the peak area count CSV and the spectra count CSV. The job name will be extracted from here')
  .option('-y, --processed_data_dir <path>', 'the path to the folder inside the raw data folder where the\n\t\t\t\t\t' +
      'pre processed data (MGFs) were stored.')
  .option('-z, --mz_tolerance [x]', 'the tolerance in Da that determines if two spectra will be compared and ' +
    'possibly joined by merging the clustering counts and keeping the fragmentation list of the most intense spectrum', parseFloat, 0.025)
  .option('-t, --rt_tolerance [x]', 'tolerance in seconds for the retention time width of the precursor ' +
    'that determines if two spectra will be compared. It enlarges the peak boundaries to deal with not aligned samples or variant spectra.', parseFloat, "2")
  .option('-a, --ion_mode [x]', 'the precursor ion mode. One of the following numeric values corresponding ' +
    'to a ion adduct type: \'1\' = [M+H]+ or \'2\' = [M-H]-', convertIonMode,1)
  .option('-s, --similarity [x]', 'the similarity to be consider when joining clusters and merging ' +
    'their counts in not blank samples', parseFloat, 0.55)
  .option('-g, --similarity_blank [x]', 'the similarity to be consider when joining clusters and ' +
    'merging their counts in blank samples (column SAMPLE\\_TYPE equals \'blank\' in the metadata table)', parseFloat, 0.3)
  .option('-w, --similarity_mn [x]', 'the minimum similarity score that must occur between a pair of ' +
      'consensus MS/MS spectra in order to create an edge in the molecular networking. Lower values will increase the ' +
      'component size of the clusters by inducing the connection of less related MS/MS spectra; and higher values will ' +
      ' limit the components sizes to the opposite', parseFloat, 0.6)
  .option('-f, --fragment_tolerance [x]', 'the tolerance in Daltons for fragment peaks. Peaks in a cluster ' +
    'spectrum that are closer than this are considered the same. The tolerance for removing the list of m/zs from the mz_exclusion ' +
    'list is one order of magnitude smaller than x.', parseFloat, 0.05)
  .option('-r, --rules [x]', 'path to the  CSV file with the accepted spectra modifications rules. ' +
    ' To be used in the spectra annotation of Step 5',"rules/np3_modifications.csv")
  .option('-c, --scale_factor [x]', 'the scaling method to be used in the fragmented peak\'s\n\t\t\t\t\t' +
      'intensities before any dot product comparision (Step 6 and 7).\n\t\t\t\t\t' +
      'Valid values are: 0 for the natural logarithm (ln) of the\n\t\t\t\t\t' +
      'intensities; 1 for no scaling; and other values greater\n\t\t\t\t\t' +
      'than zero for raising the fragment peaks intensities to\n\t\t\t\t\t' +
      'the power of x (e.g. x = 0.5 is the square root scaling).\n\t\t\t\t\t' +
      '[x] >= 0', parseFloat, 0.5)
  .option('-k, --net_top_k [x]', 'Maximum number of connection for one single node in the ' +
    'similarity molecular networking. The edge between two nodes are kept only if both nodes are within each other\'s ' +
    'net_top_k most similar nodes. This restriction is applied in the SCANS\'s order, so smaller SCANS are ' +
    'limited first. Keeping this value low makes very large networks (many nodes) much easier to visualize. ',10)
  .option('-l, --parallel_cores [x]', 'the number of cores to be used for parallel processing. ' +
      'x = 1 for disabling parallelization and x > 2 for enabling it. [x] >= 1', parseDecimal, 2)
  .option('-e, --method [name]', 'a character string indicating which correlation coefficient is to be computed. One ' +
    'of "pearson", "kendall", or "spearman"', convertMethodCorr,"spearman")
  .option('-i, --metfrag_identification [x]', 'a logical "TRUE" or "FALSE" indicating if the MetFrag tool\n\t\t\t\t\t' +
        'should be used for identification search against the\n\t\t\t\t\t' +
        'PubChem database of the top correlated m/z\'s\n\t\t\t\t\t', "FALSE")
  .option('-j, --tremolo_identification [x]', 'for not Windows OS\'s. A logical "TRUE" or "FALSE" indicating if the Tremolo tool should be used ' +
      'for the spectral matching against the In-Silico predicted MS/MS spectrum of Natural Products Database (ISDB) from the UNPD ' +
      '(Universal Natural Products Database).', "FALSE")
  .option('-b, --max_chunk_spectra [x]', "Maximum number of spectra (rows) to be loaded and processed in " +
      "a chunk at the same time. In case of memory issues this value should be decreased",parseDecimal,3000)
  .option('-v, --verbose [x]', 'for values X>0 show the scripts output information', parseDecimal, 0)
  .action(function(options) {
    if (typeof options.metadata === 'undefined') {
      console.error('\nMissing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.output_path === 'undefined') {
      console.error('\nMissing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.processed_data_dir === 'undefined') {
      console.error('\nMissing the mandatory \'processed_data_dir\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (isNaN(options.fragment_tolerance) || options.fragment_tolerance < 0)
    {
      console.error('\nERROR. Wrong fragment_tolerance parameter value. The fragment tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    if (isNaN(options.mz_tolerance) || options.mz_tolerance < 0) {
      console.error('\nERROR. Wrong mz_tolerance parameter value. The m/z tolerance must be a positive numeric value given in Daltons. Execution aborted.');
      process.exit(1);
    }
    var output_name = basename(options.output_path, osSep());

    // check if pairwise table exists
    if (!shell.test('-e', options.output_path + "/molecular_networking/similarity_tables/similarity_table_" +
        output_name + ".csv")) {
      // cread molecular networking output dir
      shell.mkdir("-p", options.output_path + "/molecular_networking/similarity_tables");
      // call pairwise comparision, create folder
      callPairwiseComparision(basename(options.output_path, osSep()), options.output_path + "/molecular_networking/similarity_tables",
          options.output_path + "/mgf/", options.fragment_tolerance,
          options.scale_factor, "TRUE", options.parallel_cores, options.verbose);
    }

    var start = process.hrtime();
    console.log('*** NP3 Clean and Annotate Clustered Spectra ***\n');

    // remove mass dissipation and anotate clustering from area and spectra count
    var resExec = callCleanAnnotateClustering(options, options.output_path, options.mz_tolerance,
      options.rt_tolerance, options.fragment_tolerance, options.processed_data_dir);

    if (!resExec ) {   // clean worked, if not windows run tremolo
      var output_clean_path = options.output_path + osSep() + "count_tables" + osSep() + "clean" + osSep();

      // for analysing the clean clustering count
      callAnalyseCount(output_clean_path+ output_name + "_spectra_clean_annotated.csv",
          output_clean_path + "analyseCountClusteringClean" );

      // tremolo search for not windows OS
      if (!isWindows() && options.tremolo_identification === "TRUE")
      {
        tremoloIdentification(output_name, options.output_path + "/identifications",
            options.output_path+"/mgf/"+output_name+"_all.mgf",
            options.mz_tolerance,0.2, 10, options.verbose, 0);
        renameTremoloJoinedIds(output_clean_path+ output_name + "_spectra_clean_annotated.csv",
            options.output_path+ "/identifications/tremolo_results.csv", options.verbose);
        mergeTremoloResults(options.output_path + "/identifications/tremolo_results.csv",
            10, [output_clean_path + output_name+"_spectra_clean_annotated.csv",
              output_clean_path + output_name+"_peak_area_clean_annotated.csv"]);
      } else if (!isWindows() && options.tremolo_identification === "FALSE" &&
          shell.test('-e', options.output_path + "/identifications/tremolo_results.csv")) {
        // do not run tremolo but its results were already created, merge it
        mergeTremoloResults(options.output_path + "/identifications/tremolo_results.csv",
            10, [output_clean_path + output_name+"_spectra_clean_annotated.csv",
              output_clean_path + output_name+"_peak_area_clean_annotated.csv"]);
      }

      // call correlation for the cleaned peak area count and spectra area count
      callComputeCorrelation(options.metadata, output_clean_path + output_name + "_peak_area_clean_annotated.csv",
        options.method,0, options.verbose);

      callComputeCorrelation(options.metadata, output_clean_path + output_name + "_spectra_clean_annotated.csv",
        options.method,0, options.verbose);

      // create MNs
      callCreatMN(options.output_path+osSep(), options.similarity_mn, options.net_top_k,
          options.max_chunk_spectra, options.verbose);

      if (options.metfrag_identification === "TRUE")
      {
        callMetfragPubChem(options.output_name, options.output_path, options.method,
            options.ion_mode, 3, options.fragment_tolerance,
            options.scale_factor, options.verbose);
      }
    }

    console.log(printTimeElapsed(process.hrtime(start)));

    if (options.verbose >= 10) {
      if (test_res[options.output_name] === undefined) {
        test_res[options.output_name] = "";
      }
      test_res[options.output_name] = test_res[options.output_name] + checkCleanMNConsistency(options.output_path,options.similarity, options.similarity_mn,
          options.rt_tolerance, options.mz_tolerance, options.net_top_k);

      test_res[options.output_name] = test_res[options.output_name] + checkCountsConsistency(options.output_path,options.processed_data_dir,
          options.metadata, false, true, false);
    }
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js clean_annotate --metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --output_path "/path/to/the/output/dir/test_np3/outs/test_np3"');
  });

program
  .command('merge')
  .description('Step 8: Run the script to merge the clean counts based on the annotations. It creates new symbolic ' +
      'candidates representing the union of the ions variants\n\n')
  .option('-o, --output_path <path>', 'path to the output data folder, inside the outs directory of the clustering result folder. ' +
      'It should contain the counts_table folder and inside it the clean folder. The job name will be extracted from here')
  .option('-y, --processed_data_dir <path>', 'the path to the folder inside the raw data folder where the\n\t\t\t\t\t' +
        'pre processed data (MGFs) were stored.')
  // .option('-b, --merge_annotations [x]', 'The annotations types to be used to merge the count tables (Step 8). ' +
  //     ' They must be provided in a single string separated by a comma, available types are: adducts, isotopes, dimers, ' +
  //     'multicharges or fragments',"adducts,isotopes,dimers,multicharges,fragments")
  .option('-m, --metadata <path>', 'path to the metadata table CSV file to be used for the correlations.')
  .option('-e, --method [name]', 'a character string indicating which correlation coefficient is to be computed. One ' +
      'of "pearson", "kendall", or "spearman"', convertMethodCorr,"spearman")
  .option('-v, --verbose [x]', 'for values X>0 show the scripts output information', parseDecimal, 0)
  .action(function(options) {
    if (typeof options.output_path === 'undefined') {
      console.error('\nMissing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.processed_data_dir === 'undefined') {
      console.error('\nMissing the mandatory \'processed_data_dir\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.metadata === 'undefined') {
      console.error('\nMissing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }

    var start = process.hrtime();
    // run workflow
    console.log('*** NP3 Merge Counts based on Annotations ***\n');

    // merge double charge and dimer annotation
    callMergeCounts(options.output_path, basename(options.output_path, osSep()),
        options.processed_data_dir, options.metadata, options.method, options.verbose);

    console.log(printTimeElapsed(process.hrtime(start)));

    if (options.verbose >= 10) {
      output_name = basename(options.output_path, osSep());
      if (test_res[output_name] === undefined) {
        test_res[output_name] = "";
      }
      test_res[output_name] = test_res[output_name] + checkCountsConsistency(options.output_path,options.processed_data_dir,
          options.metadata, false, false, true);
    }
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js merge --output_path "/path/to/the/output/dir/test_np3/outs/test_np3" ');
  });

program
    .command('corr')
    .description('Step 9: Run the bioactivity correlation script to rank the mass candidates based on the scores for ' +
        'the selection of samples from the metadata\n\n')
    .option('-m, --metadata <file>', 'path to the metadata table CSV file')
    .option('-c, --count_file <file>', 'path to the count CSV file')
    .option('-e, --method [name]', 'a character string indicating which correlation coefficient is to be computed. One ' +
        'of "pearson", "kendall", or "spearman"', convertMethodCorr,"spearman")
    .option('-b, --bio_cutoff [name]', 'a bioactivity cutoff value. Bioactivities in the metadata table ' +
        'that are less than the bio_cutoff value will be set to zero.', parseDecimal,0)
    .option('-v, --verbose [x]', 'for values X>0 show the scripts output information', parseDecimal, 0)
    .action(function(options) {
      if (typeof options.metadata === 'undefined') {
        console.error('Missing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
        process.exit(1);
      }
      if (typeof options.count_file === 'undefined') {
        console.error('Missing the mandatory \'count_file\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
        process.exit(1);
      }

      var start = process.hrtime();
      // run workflow
      console.log('*** NP3 Spectra Count and Bioactivity Correlation ***\n');

      // call correlation for the mscluster count
      callComputeCorrelation(options.metadata, options.count_file, options.method, options.bio_cutoff, options.verbose);

      console.log(printTimeElapsed(process.hrtime(start)));
    })
    .on('--help', function() {
      console.log('');
      console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
      console.log('');
      console.log('Results:');
      console.log('');
      console.log('A CSV table in the count_file directory named as \'<count_file>_corr_<method>.csv\', without the count_file extension, containing the count table, ' +
          'plus a new column for each correlation group and bioactivity score defined in the metadata table, ' +
          'with the correlation score of each m/z, plus one or more rows with the bioactivities scores of each sample placed above the counts table header (first rows).\n' +
          '\n' +
          'In the metadata table the columns with a bioactivity score must be named with the prefix "BIOACTIVITY_" and ' +
          'the columns with the correlation groups (samples to be used in each correlation) must be named with the prefix "COR_".\n' +
          'The correlation score will produce NA values with warnings if the counts of the selected samples are all equal 0 or ' +
          'if the bioactivity of the selected samples are all the same, and it will produce "CTE" if the counts of the selected samples ' +
          'have the same values (standard deviation equals 0). ');
      console.log('');
      console.log('Examples:');
      console.log('');
      console.log('  $ node np3_workflow.js corr --metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --count_file "/path/to/the/metadata/file/np3_job_spectra.csv"')
    });

program
    .command('mn')
    .description('Step 10: Run the script to create two molecular networkings, one based on spectra similarity and other based ' +
        'on the spectra annotations, enhancing library and chemical desreplication\n\n')
    .option('-o, --output_path <path>', 'path to the output data folder, inside the outs directory of the clustering result folder. ' +
        'It should contain the molecular_networking folder and inside it the similarity_tables folder. The job name will be extracted from here')
    .option('-w, --similarity_mn [x]', 'the minimum similarity score that must occur between a pair of ' +
        'consensus MS/MS spectra in order to create an edge in the molecular networking. Lower values will increase the ' +
        'component size of the clusters by inducing the connection of less related MS/MS spectra; and higher values will ' +
        ' limit the components sizes to the opposite', parseFloat, 0.6)
    .option('-k, --net_top_k [x]', 'Maximum number of connection for one single node in the ' +
        'similarity molecular networking. The edge between two nodes are kept only if both nodes are within each other\'s ' +
        'net_top_k most similar nodes. This restriction is applied in the SCANS\'s order, so smaller SCANS are ' +
        'limited first. Keeping this value low makes very large networks (many nodes) much easier to visualize. ',10)
    .option('-b, --max_chunk_spectra [x]', "Maximum number of spectra (rows) to be loaded and processed in " +
        "a chunk at the same time. In case of memory issues this value should be decreased",parseDecimal,3000)
    .option('-v, --verbose [x]', 'for values X>0 show the scripts output information', parseDecimal, 0)
    .action(function(options) {
      if (typeof options.output_path === 'undefined') {
        console.error('\nMissing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
        process.exit(1);
      }

      var start = process.hrtime();
      // run workflow
      console.log('*** NP3 Molecular Networking Creation ***\n');

      callCreatMN(options.output_path, options.similarity_mn, options.net_top_k,
          options.max_chunk_spectra, options.verbose);

      console.log(printTimeElapsed(process.hrtime(start)));

      if (options.verbose >= 10) {
        if (test_res[output_name] === undefined) {
          test_res[output_name] = "";
        }
        test_res[output_name] = test_res[output_name] + checkMNConsistency(options.output_path, options.similarity_mn, options.net_top_k);
      }
    })
    .on('--help', function() {
      console.log('');
      console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
      console.log('');
      console.log('Examples:');
      console.log('');
      console.log('  $ node np3_workflow.js mn --output_path "/path/to/the/output/dir/test_np3/outs/test_np3" ' +
          '--similarity_mn 0.7 --verbose 1');
    });

program
  .command('metfrag')
  .description('An interactive prompt to run the MetFrag tool for identification search of individual spectra or of ' +
    'an entire experiment against the PubChem database\n\n')
  .option('-g, --mgf <path>', 'complete path to the input MGF file with MS/MS spectra data')
  .option('-o, --output_path <path>', 'path to where the identification results will be saved. ' +
    'Prefereable inside the final clustering results directory in the \'job_name/outs/job_name/identifications\' folder')
  .action(function(options) {
    if (typeof options.mgf === 'undefined') {
      console.error('Missing the mandatory \'mgf\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.output_path === 'undefined') {
      console.error('Missing the mandatory \'output_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }

    const { execFileSync } = require('child_process');
    // run workflow
    console.log('*** NP3 Comparing Spectra ***');

    try {
      var resExec = execFileSync("Rscript", ["src/metfrag_interactivesearch.R", options.mgf, options.output_path],
        {stdio: 'inherit'});
    } catch (err) {
      console.log('\nERROR');
      //console.log(err.toString().trim());
      process.exit(1);
    }

    console.log('\nDONE!\n');
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js metfrag --mgf "/path/to/the/mgf/file/input_search.mgf" --output_path "/path/to/the/output/directory/job_name/outs/job_name/identifications"');
    console.log('');
    console.log('  $ node np3_workflow.js metfrag --g "/path/to/the/mgf/file/input_search.mgf" -o "/path/to/the/output/directory/job_name/outs/job_name/identifications"');
  });

program
  .command('chr')
  .description('An interactive prompt to extract chromatogram(s) from raw data files (mzXML, mzData and mzML) and to ' +
    'save to PNG image files. Depending on the provided parameters this can be a total ion chromatogram (TIC - default), ' +
    'a base peak chromatogram (BPC) or an extracted ion chromatogram (XIC) extracted from each sample/file\n\n')
  .option('-n, --data_name [name]', 'the data collection name. Used for verbosity', "X")
  .option('-m, --metadata <file>', 'path to the metadata table CSV file')
  .option('-d, --raw_data_path <path>', 'path to the folder containing the input MS/MS spectra raw data in mzXML, mzData and mzML format')
  .action(function(options) {
    if (typeof options.metadata === 'undefined') {
      console.error('Missing the mandatory \'metadata\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }
    if (typeof options.raw_data_path === 'undefined') {
      console.error('Missing the mandatory \'raw_data_path\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }

    const { execFileSync } = require('child_process');
    // run workflow
    console.log('*** NP3 Extracting Chromatograms ***');

    // call extract chromatogram using xcms
    //var resExec = shell.exec('Rscript src/chromatogram_xcms.R '+options.data_name+' '+options.metadata+' '
    //  +options.raw_data_path, {async:false});

    try {
      var resExec = execFileSync("Rscript", ["src/chromatogram_xcms.R", options.data_name, options.metadata,
        options.raw_data_path], {stdio: 'inherit'});
    } catch (err) {
      console.log('\nERROR');
      //console.log(err.toString().trim());
      process.exit(1);
    }

    console.log('\nDONE!\n');
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Results:');
    console.log('');
    console.log('PNG image(s) file(s) with the extracted chromatogram(s) of the selected sample(s) and grouped as specified in the arguments.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js chr --metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --data_name "data_np3" --raw_data_path "/path/to/the/raw/data/directory"');
    console.log('');
    console.log('  $ node np3_workflow.js chr --b "/path/to/the/metadata/file/test_np3_metadata.csv" -d "/path/to/the/raw/data/directory"');
  });

program
  .command('compare_spectra')
  .description('An interactive prompt to compare two spectra from a MGF file, to plot them against it other and ' +
      'to save to a PNG image file\n\n')
  .option('-n, --data_name [name]', 'the data collection name. Used for verbosity', "-")
  .option('-g, --mgf <path>', 'complete path to the input MGF file with MS/MS spectra data')
  .action(function(options) {
    if (typeof options.mgf === 'undefined') {
      console.error('Missing the mandatory \'mgf\' parameter. See --help for the list of mandatory parameters indicated by angled brackets (e.g. <value>).');
      process.exit(1);
    }

    const { execFileSync } = require('child_process');
    // run workflow
    console.log('*** NP3 Comparing Spectra ***');

    try {
      var resExec = execFileSync("Rscript", ["src/compare_spectra.R", options.data_name, options.mgf], {stdio: 'inherit'});
    } catch (err) {
      console.log('\nERROR');
      //console.log(err.toString().trim());
      process.exit(1);
    }

    console.log('\nDONE!\n');
  })
  .on('--help', function() {
    console.log('');
    console.log('Angled brackets (e.g. <x>) indicate required input. Square brackets (e.g. [y]) indicate optional input.');
    console.log('');
    console.log('Examples:');
    console.log('');
    console.log('  $ node np3_workflow.js chr --metadata "/path/to/the/metadata/file/test_np3_metadata.csv" --data_name "data_np3" --raw_data_path "/path/to/the/raw/data/directory"');
    console.log('');
    console.log('  $ node np3_workflow.js chr --b "/path/to/the/metadata/file/test_np3_metadata.csv" -d "/path/to/the/raw/data/directory"');
  });


const runCommand = (alias, args = []) => {
  program._events[`command:${alias}`]([],args);
};

// TODO: add trycatch in each test case
program
  .command('test')
  .description('Run some cases to test the workflow consistency in all steps.')
  .option('-p, --pre_process [x]', '\'TRUE\' or \'FALSE\' to test the pre process step.', "FALSE")
  .option('-t, --tremolo [x]', '\'TRUE\' or \'FALSE\' to test the tremolo step.', "FALSE")
  .option('-s, --skip [x]', 'Skip to test x', 1)
  .action(function(options) {
    var start = process.hrtime();

    var args_cmd = {fragment_tolerance: 0.05,
      mz_tolerance: 0.025,
      ppm_tolerance: 15,
      ion_mode: 1,
      similarity: 0.55,
      similarity_blank: 0.3,
      similarity_mn: 0.6,
      rt_tolerance: [ 1, 2 ],
      net_top_k: 10,
      scale_factor: 0.5,
      parallel_cores: 2,
      processed_data_name: 'processed_data',
      processed_data_overwrite: options.pre_process,
      rules: 'rules/np3_modifications.csv',
      trim_mz: 'TRUE',
      min_peaks_output: 5,
      max_chunk_spectra: 3000,
      method: 'spearman',
      metfrag_identification: 'FALSE',
      tremolo_identification: options.tremolo,
      verbose: 10,
      parent : {'rawArgs' : []} };

    // # test metadata with only blank samples; recompute preprocess
    // # not using parallel in the pairwise

    args_cmd.output_name = 'Bra_test_blanks';
    args_cmd.metadata = 'test/Bra346/Bra346_METADATA_blanks.csv';
    args_cmd.raw_data_path = 'test/Bra346/mzxml';
    args_cmd.output_path = 'test/Bra346/';
    args_cmd.processed_data_name = 'processed_data_blanks';
    args_cmd.parallel_cores = 1;
    if (options.skip <= 1) {
      shell.rm('-rf', 'test/Bra346/Bra_test_blanks');
      console.log("*** Test 1 - Bra_test_blanks - run ***\n\n");
      runCommand('run', [args_cmd]);
      console.log("\n\n");
    }

    // test metadata with only samples, no other type;
    args_cmd = {fragment_tolerance: 0.05,
      mz_tolerance: 0.025,
      ppm_tolerance: 15,
      ion_mode: 1,
      similarity: 0.55,
      similarity_blank: 0.3,
      similarity_mn: 0.6,
      rt_tolerance: [ 1, 2 ],
      net_top_k: 10,
      scale_factor: 0.5,
      parallel_cores: 2,
      processed_data_name: 'processed_data',
      processed_data_overwrite: options.pre_process,
      rules: 'rules/np3_modifications.csv',
      trim_mz: 'TRUE',
      min_peaks_output: 5,
      max_chunk_spectra: 3000,
      method: 'spearman',
      metfrag_identification: 'FALSE',
      tremolo_identification: options.tremolo,
      verbose: 10,
      parent : {'rawArgs' : []}};

    args_cmd.output_name = 'Bra_test_samples';
    args_cmd.metadata = 'test/Bra346/Bra346_METADATA_samples.csv';
    args_cmd.raw_data_path = 'test/Bra346/mzxml';
    args_cmd.output_path = 'test/Bra346/';
    args_cmd.processed_data_name = 'processed_data_samples';
    if (options.skip <= 2) {
      shell.rm('-rf', 'test/Bra346/Bra_test_samples');
      console.log("*** Test 2 - Bra_test_samples - run ***\n\n");
      runCommand('run', [args_cmd]);
      console.log("\n\n");
    }

    // # test metadata with more than 10 samples in one data collection ranging all types except blanks;
    args_cmd = {fragment_tolerance: 0.05,
      mz_tolerance: 0.025,
      ppm_tolerance: 15,
      ion_mode: 1,
      similarity: 0.55,
      similarity_blank: 0.3,
      similarity_mn: 0.6,
      rt_tolerance: [ 1, 2 ],
      net_top_k: 10,
      scale_factor: 0.5,
      parallel_cores: 2,
      processed_data_name: 'processed_data',
      processed_data_overwrite: options.pre_process,
      rules: 'rules/np3_modifications.csv',
      trim_mz: 'TRUE',
      min_peaks_output: 5,
      max_chunk_spectra: 3000,
      method: 'spearman',
      metfrag_identification: 'FALSE',
      verbose: 10,
      parent : {'rawArgs' : ['node np3_workflow.js run -n Bra_test_one_collection_10_samples -m test/Bra346/Bra346_METADATA_one_collection_11_samples.csv -d test/Bra346/mzxml -o test/Bra346/ -y processed_data_one_collection_11_samples -j TRUE -v 11']}};

    args_cmd.output_name = 'Bra_test_one_collection_10_samples';
    args_cmd.metadata = 'test/Bra346/Bra346_METADATA_one_collection_10_samples.csv';
    args_cmd.raw_data_path = 'test/Bra346/mzxml';
    args_cmd.output_path = 'test/Bra346/';
    args_cmd.processed_data_name = 'processed_data_one_collection_10_samples';
    args_cmd.tremolo_identification = options.tremolo;
    if (options.skip <= 3) {
      shell.rm('-rf', 'test/Bra346/Bra_test_one_collection_10_samples');
      console.log("*** Test 3 - Bra_test_one_collection_10_samples - run ***\n\n");
      runCommand('run', [args_cmd]);
      console.log("\n\n");
    }

    // # test metadata with more than 10 samples in more than 10 data collections with samples and blanks
    args_cmd = {fragment_tolerance: 0.05,
      mz_tolerance: 0.025,
      ppm_tolerance: 15,
      ion_mode: 1,
      similarity: 0.55,
      similarity_blank: 0.3,
      similarity_mn: 0.6,
      rt_tolerance: [ 1, 2 ],
      net_top_k: 10,
      scale_factor: 0.5,
      parallel_cores: 2,
      processed_data_name: 'processed_data',
      processed_data_overwrite: options.pre_process,
      rules: 'rules/np3_modifications.csv',
      trim_mz: 'TRUE',
      min_peaks_output: 5,
      max_chunk_spectra: 3000,
      method: 'spearman',
      metfrag_identification: 'FALSE',
      tremolo_identification: options.tremolo,
      verbose: 10,
      parent : {'rawArgs' : ['node np3_workflow.js run -n Bra_test_multi_collection_10_samples -m test/Bra346/Bra346_METADATA_multi_collection_10_samples.csv -d test/Bra346/mzxml -o test/Bra346/ -y processed_data_multi_collection_15_samples -j TRUE -v 11']}};

    args_cmd.output_name = 'Bra_test_multi_collection_10_samples';
    args_cmd.metadata = 'test/Bra346/Bra346_METADATA_multi_collection_10_samples.csv';
    args_cmd.raw_data_path = 'test/Bra346/mzxml';
    args_cmd.output_path = 'test/Bra346/';
    args_cmd.processed_data_name = 'processed_data_multi_collection_10_samples';
    if (options.skip <= 4) {
      shell.rm('-rf', 'test/Bra346/Bra_test_multi_collection_10_samples');
      console.log("*** Test 4 - Bra_test_multi_collection_10_samples - run ***\n\n");
      runCommand('run', [args_cmd]);
      console.log("\n\n");
    }

    // # one sample per type, each one in a separate data collection
    // # split the run cmd in the sub cmds calls and using a smaller chunk size

    if (options.pre_process == "TRUE" && options.skip <= 5) {
      args_cmd = {data_name: "Bra_test_types",
        processed_data_name: 'processed_data_types',
        rt_tolerance: 2,
        mz_tolerance: 0.05,
        ppm_tolerance: 15,
        ion_mode: 1,
        peak_width: '2,5',
        snthresh: 0,
        pre_filter: '1,750',
        min_fraction: 0.3,
        bw: 1,
        bin_size: 0.1,
        max_features: 100,
        noise: 500,
        mz_center_fun: 'wMeanApex3',
        max_samples_batch_align: 5,
        integrate_method: 2,
        fit_gauss: 'FALSE',
        processed_data_overwrite: options.pre_process,
        verbose: 10,
        parent : {'rawArgs' : ['node np3_workflow.js preprocess -n Bra_test_types -m test/Bra346/Bra346_METADATA_types.csv -d test/Bra346/mzxml -y processed_data_types -v 13']}};
      args_cmd.metadata = 'test/Bra346/Bra346_METADATA_types.csv';
      args_cmd.raw_data_path = 'test/Bra346/mzxml';
      console.log("*** Test 5 - Bra_test_types - pre_process ***\n\n");
      runCommand('pre_process', [args_cmd]);
      console.log("\n\n");


    }

    args_cmd = {fragment_tolerance: 0.05,
      mz_tolerance: 0.025,
      ppm_tolerance: 15,
      ion_mode: 1,
      similarity: 0.55,
      similarity_blank: 0.3,
      similarity_mn: 0.6,
      rt_tolerance: [ 1, 2 ],
      net_top_k: 10,
      scale_factor: 0.5,
      parallel_cores: 2,
      processed_data_name: 'processed_data',
      processed_data_overwrite: options.pre_process,
      rules: 'rules/np3_modifications.csv',
      trim_mz: 'TRUE',
      min_peaks_output: 5,
      max_chunk_spectra: 500,
      method: 'spearman',
      metfrag_identification: 'FALSE',
      tremolo_identification: options.tremolo,
      verbose: 10,
      parent : {'rawArgs' : ['node np3_workflow.js clustering -n Bra_test_types -m test/Bra346/Bra346_METADATA_types.csv -d test/Bra346/mzxml -o test/Bra346 -y processed_data_types -v 13 -b 500']}};

    args_cmd.output_name = 'Bra_test_types';
    args_cmd.metadata = 'test/Bra346/Bra346_METADATA_types.csv';
    args_cmd.raw_data_path = 'test/Bra346/mzxml';
    args_cmd.output_path = 'test/Bra346/';
    args_cmd.processed_data_name = 'processed_data_types';
    if (options.skip <= 6) {
      shell.rm('-rf', 'test/Bra346/Bra_test_types');
      console.log("*** Test 6 - Bra_test_types - clustering ***\n\n");
      runCommand('clustering', [args_cmd]);
      console.log("\n\n");
    }

    args_cmd.output_path = 'test/Bra346/Bra_test_types/outs/Bra_test_types/';
    args_cmd.processed_data_dir = 'test/Bra346/mzxml/processed_data_types/';
    args_cmd.mgf = 'test/Bra346/Bra_test_types/outs/Bra_test_types/mgf/Bra_test_types_all.mgf';
    args_cmd.top_k = 20;
    args_cmd.parent.rawArgs = 'node np3_workflow.js tremolo -o test/Bra346/Bra_test_types/outs/Bra_test_types/identifications/ -g test/Bra346/Bra_test_types/outs/Bra_test_types/mgf/Bra_test_types_all.mgf -c test/Bra346/Bra_test_types/outs/Bra_test_types/count_tables/clean/Bra_test_types_spectra_clean_annotated.csv,test/Bra346/Bra_test_types/outs/Bra_test_types/count_tables/clean/Bra_test_types_peak_area_clean_annotated.csv -k 20 -v 13';
    if (options.skip <= 7) {
      console.log("*** Test 7 - Bra_test_types - tremolo ***\n\n");
      runCommand('tremolo', [args_cmd]);
    }

    args_cmd.rt_tolerance = 2;
    args_cmd.parent.rawArgs = 'node np3_workflow.js clean_annotate -m test/Bra346/Bra346_METADATA_types.csv -o test/Bra346/Bra_test_types/outs/Bra_test_types -y test/Bra346/mzxml/processed_data_types -b 500 -v 13';
    if (options.skip <= 8) {
      console.log("*** Test 8 - Bra_test_types - clean_annotate ***\n\n");
      runCommand('clean_annotate', [args_cmd]);
    }

    args_cmd.parent.rawArgs = 'node np3_workflow.js merge -o test/Bra346/Bra_test_types/outs/Bra_test_types -y test/Bra346/mzxml/processed_data_types -m test/Bra346/Bra346_METADATA_types.csv -v 13';
    if (options.skip <= 9) {
      console.log("*** Test 9 - Bra_test_types - merge ***\n\n");
      runCommand('merge', [args_cmd]);
    }
    args_cmd.parent.rawArgs = 'node np3_workflow.js mn -o test/Bra346/Bra_test_types/outs/Bra_test_types -w 0.7 -k 15 -b 200 -v 13';
    args_cmd.similarity_mn = 0.7;
    args_cmd.net_top_k = 15;
    args_cmd.max_chunk_spectra = 200;
    if (options.skip <= 10) {
      console.log("\n*** Test 10 - Bra_test_types - mn ***\n\n");
      runCommand('mn', [args_cmd]);
      console.log("\n\n\n");
    }
    console.log("-------------------------------------------------------\n");
    console.log("-------------------------TEST--------------------------\n");
    console.log("-------------------------------------------------------\n\n");

    console.log(test_res.Bra_test_blanks);
    console.log(test_res.Bra_test_samples);
    console.log(test_res.Bra_test_one_collection_10_samples);
    console.log(test_res.Bra_test_multi_collection_10_samples);
    console.log(test_res.Bra_test_types);

    console.log("\n-------------------------------------------------------\n");
    console.log(printTimeElapsed(process.hrtime(start)));
    console.log("\n--------------------------END--------------------------\n");
  });

// error on unknown commands
program.on('command:*', function () {
  console.error('Invalid command: %s\nSee --help for a list of available commands.', program.args.join(' '));
  process.exit(1);
});

program.parse(process.argv);